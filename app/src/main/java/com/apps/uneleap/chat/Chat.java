package com.apps.uneleap.chat;

import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ChatAdapter;
import com.apps.uneleap.databinding.ActivityChatBinding;

public class Chat extends AppCompatActivity {

    ActivityChatBinding activityChatBinding;
    ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chat);
        activityChatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        activityChatBinding.chatRV.setLayoutManager(new LinearLayoutManager(this));
        chatAdapter = new ChatAdapter(this);
        activityChatBinding.chatRV.setAdapter(chatAdapter);
    }
}
