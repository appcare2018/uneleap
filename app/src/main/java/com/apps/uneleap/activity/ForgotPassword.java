package com.apps.uneleap.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.apps.uneleap.R;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ActivityForgotPasswordBinding;
import com.apps.uneleap.databinding.ActivityLoginBinding;
import com.apps.uneleap.model.ForgetPasswordResponseModel;
import com.apps.uneleap.model.LoginResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.util.AppUtil.startActivityWithAnimation;

public class ForgotPassword extends AppCompatActivity implements ApiResponseErrorCallback {

    private ActivityForgotPasswordBinding binding;
    private CommonFunctions cmf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        cmf = new CommonFunctions(this);
    }

    public void on_click(View view) {
        sendToApi();
    }

    private void sendToApi() {

        JSONObject params = new JSONObject();
        try {

            params.put("username", binding.etEmail.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.forgetPassword(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, 1,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {

        ForgetPasswordResponseModel model = (ForgetPasswordResponseModel)object;
        if (model.getStatus()==200){
            //startActivityWithAnimation(Login.this, new Intent(getApplicationContext(),MainActivity.class));
            Utils.showToast(this,model.getMessage());
            binding.tempPassword.setText("Your Temporary Password is: "+model.getTemporary_password());
        }
        else {
            Utils.showToast(this,model.getMessage());

        }

    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
