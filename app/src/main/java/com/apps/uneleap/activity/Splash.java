package com.apps.uneleap.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.apps.uneleap.R;
import com.apps.uneleap.databinding.ActivitySplashBinding;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.viewpager.ViewPagerActivity;


import static com.apps.uneleap.util.AppUtil.rotate360;
import static com.apps.uneleap.util.GlobalConstants.IS_LOGIN;


public class Splash extends AppCompatActivity {

    private ActivitySplashBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        WindowManager.LayoutParams attrib = getWindow().getAttributes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        intitView();
    }

    private void intitView() {
        rotate360(this,binding.logo);

        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (CommonFunctions.myPreference.getBoolean(Splash.this, IS_LOGIN)) {
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        startActivity(intent);
                        finishAffinity();
                    }
                    else {
                        Intent intent = new Intent(Splash.this, ViewPagerActivity.class);
                        startActivity(intent);
                        finishAffinity();
                    }
                }
            }
        };
        timerThread.start();
    }
}
