package com.apps.uneleap.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.apps.uneleap.R;
import com.apps.uneleap.activity.profile.Profile;
import com.apps.uneleap.databinding.ActivityToolsBinding;
import com.apps.uneleap.util.Utils;

public class Tools extends AppCompatActivity {

    private ActivityToolsBinding binding;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_tools);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tools);
        binding.profilePic.getBorderColor();
        binding.tvProfileName.setOnClickListener(v -> {
            startActivity(new Intent(Tools.this, Profile.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });
        binding.tvViewProfile.setOnClickListener(v -> {
            startActivity(new Intent(Tools.this, Profile.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        binding.ivSettings.setOnClickListener(v -> {
            startActivity(new Intent(Tools.this, Settings.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });


    }

    public void logout(View view) {
        FragmentManager fm = this.getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        //finish();
        //startActivity( new Intent(getApplicationContext(),GetStarted.class));
        Utils.logoutUser( this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;

                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    // Left to Right swipe action
                    if (x2 > x1)
                    {
                        //Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                    }

                    // Right to left swipe action
                    else
                    {
                        onBackPressed();
                        //Toast.makeText(this, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                    }

                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
