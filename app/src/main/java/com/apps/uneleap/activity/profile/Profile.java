package com.apps.uneleap.activity.profile;

import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ProfileExpAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ActivityProfileBinding;
import com.apps.uneleap.model.profile.ProfileEducationModel;
import com.apps.uneleap.model.profile.ProfileExperienceModel;
import com.apps.uneleap.model.profile.ProfileModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;

public class Profile extends AppCompatActivity {

    private ActivityProfileBinding binding;
    CommonFunctions cmf;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        initView();
    }


    private void initView() {
        cmf = new CommonFunctions(this);
        userId = cmf.myPreference.getString(this, USER_ID);
       // userId="1";
        binding.tvResume.setPaintFlags(binding.tvResume.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.tvResume.setText("Resume");
        binding.tvEditProfile.setOnClickListener(v -> cmf.startAc(Profile.this,EditPublicProfileAc.class,null));
        binding.tvEditExp.setOnClickListener(v -> cmf.startAc(Profile.this,EditExpActivity.class,null));
        binding.tvEditEud.setOnClickListener(v -> cmf.startAc(Profile.this,EditEudAc.class,null));

        loadData();
    }


    //--------------------------------------------------------------------------------
    ProfileModel responseModel;

    private void loadData() {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getProfile(cmf.userID(userId)).enqueue(
                    new ApiCallBack<>(Profile.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            exp_loadData();
                            responseModel = (ProfileModel) object;
                            if (responseModel.getStatusCode() == 200) {
                                if (responseModel.getData() != null) {
                                    uiUpdate();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {
                            exp_loadData();
                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void uiUpdate() {
        binding.tvProfileName.setText(responseModel.getData().getFirstName() + " " + responseModel.getData().getLastName());
        binding.tvDesignation.setText(responseModel.getData().getDesignation());
        binding.tvUniversity.setText(responseModel.getData().getUniversity());
        binding.tvAboutU.setText(responseModel.getData().getAboutYou());

        binding.tvPhone.setText(responseModel.getData().getPhoneNumber());
        binding.tvEmail.setText(responseModel.getData().getEmail());
        binding.tvWebSite.setText(responseModel.getData().getWebsite());

        Glide.with(this)
                .load(responseModel.getData().getProfilePic())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(R.drawable.profile_image)
                .error(R.drawable.profile_image)
                .into(binding.profilePic);


    }

    //--------------------------------------------------------
    ProfileExperienceModel profileExperienceModel;

    private void exp_loadData() {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getUserExperience(cmf.userID(userId)).enqueue(
                    new ApiCallBack<>(Profile.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            eud_loadData();
                            profileExperienceModel = (ProfileExperienceModel) object;
                            if (profileExperienceModel.getStatusCode() == 200) {
                                if (profileExperienceModel.getData() != null) {
                                    exp_uiUpdate();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {
                            eud_loadData();
                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void exp_uiUpdate() {
        ProfileExpAdapter profileExpAdapter=new ProfileExpAdapter(this,profileExperienceModel.getData(),R.layout.profile_experience_item,cmf);
        binding.rcvExpList.setAdapter(profileExpAdapter);
    }

    //--------------------------------------------------------
    ProfileEducationModel profileEducationModel;

    private void eud_loadData() {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getUserEducation(cmf.userID(userId)).enqueue(
                    new ApiCallBack<>(Profile.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            profileEducationModel = (ProfileEducationModel) object;
                            if (profileEducationModel.getStatusCode() == 200) {
                                if (profileEducationModel.getData() != null) {
                                    eud_uiUpdate();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void eud_uiUpdate() {
        ProfileExpAdapter profileExpAdapter=new ProfileExpAdapter(profileEducationModel.getData(),R.layout.profile_experience_item,cmf);
        binding.rcvEudList.setAdapter(profileExpAdapter);
    }
    //--------------------------------------------------------


}
