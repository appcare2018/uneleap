package com.apps.uneleap.activity.forum;

import android.os.Bundle;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ForumPageTopicsAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ForumCommentBinding;
import com.apps.uneleap.databinding.ForumTopicsBinding;
import com.apps.uneleap.model.TopicsResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;

public class ForumTopics extends AppCompatActivity implements ApiResponseErrorCallback {

    ForumTopicsBinding binding;
    private ForumPageTopicsAdapter adapterTopics;
    CommonFunctions cmf;
    private ArrayList<TopicsResponseModel.Data> listingDetails = new ArrayList<>();
    GridLayoutManager  linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forum_topics);
        cmf = new CommonFunctions(this);



        linearLayoutManager = new GridLayoutManager(this, 2 ,GridLayoutManager.VERTICAL, false);
        binding.rcvTopics.setLayoutManager(linearLayoutManager);
       getApi();
    }

    private void getApi() {

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getAllTopics().enqueue(
                    new ApiCallBack<>(this, this, 1,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {
        TopicsResponseModel responseModel = (TopicsResponseModel) object;

        if (responseModel.getStatus() == 200) {
            if (responseModel.getData() != null) {
                if (responseModel.getData().size() != 0) {

                    listingDetails = new ArrayList<>();
                    listingDetails.addAll(responseModel.getData());
                    adapterTopics = new ForumPageTopicsAdapter(this, listingDetails);
                    binding.rcvTopics.setAdapter(adapterTopics);


                }
            }
        }
    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
