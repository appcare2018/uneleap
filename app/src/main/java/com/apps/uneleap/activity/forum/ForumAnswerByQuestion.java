package com.apps.uneleap.activity.forum;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ForumAnswerByQuestionAdapter;
import com.apps.uneleap.adapter.ForumPageQuestionsAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ForumAnswerByQuestionBinding;
import com.apps.uneleap.databinding.ForumCommentBinding;
import com.apps.uneleap.model.AllTopicSpinnerModel;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.GetAnswerByQuestionResponseModel;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.ADDQUESTIONS;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.LIKEDISLIKE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;

public class ForumAnswerByQuestion extends AppCompatActivity implements ApiResponseErrorCallback {

    ForumAnswerByQuestionBinding binding;
    CommonFunctions cmf;
    int id=0;
    String question="";
    private ArrayList<GetAnswerByQuestionResponseModel.Data> questionDetails = new ArrayList<>();
    LinearLayoutManager  questionLinearLayoutManager;
    private ForumAnswerByQuestionAdapter adapterQuestion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forum_answer_by_question);
        cmf = new CommonFunctions(this);
        id = getIntent().getIntExtra("id",0);
        question = getIntent().getStringExtra("question");
        questionLinearLayoutManager = new LinearLayoutManager(this,  LinearLayoutManager.VERTICAL, false);
        binding.rcvQuestion.setLayoutManager(questionLinearLayoutManager);

        binding.tvQuestion.setText(question);

        getQuestions(id);

        binding.tvAnswerIt.setOnClickListener(view -> showAddDialog());
    }

    private void showAddDialog() {




        final Dialog dialog = new Dialog(this,R.style.MyCustomDialog);
        dialog.setCancelable(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_write_answer);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ImageView ivClose =  dialog.findViewById(R.id.ivClose);
        final Button btnAdd =  dialog.findViewById(R.id.btnAdd);
        final EditText etAns =  dialog.findViewById(R.id.etAns);


        ivClose.setOnClickListener(v -> dialog.dismiss());
        btnAdd.setOnClickListener(v -> {
            addAnswer(id,etAns.getText().toString());
            dialog.dismiss();
        });



        dialog.show();

    }



    private void addAnswer(int queId,String question){

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis();

            params.put("user_id", CommonFunctions.myPreference.getString(this, USER_ID));
            params.put("question_id", queId);
            params.put("answer", question);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.addAnswer(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, ADDQUESTIONS,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void getQuestions(int questionId) {

        JSONObject params = new JSONObject();
        try {
            params.put("user_id", CommonFunctions.myPreference.getString(this, USER_ID));
            params.put("question_id", questionId);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getAnswersByQuestionId(ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, QUESTIONS,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {
        if(flag == QUESTIONS){
            GetAnswerByQuestionResponseModel responseModel = (GetAnswerByQuestionResponseModel) object;
            if (responseModel.getStatus() == 200) {
                if (responseModel.getData() != null) {
                    if (responseModel.getData().size() != 0) {
                        questionDetails = new ArrayList<>();
                        questionDetails.addAll(responseModel.getData());



                        binding.tvAnswerCount.setText(questionDetails.size()+" Answers");



                        adapterQuestion = new ForumAnswerByQuestionAdapter(this, questionDetails, (isQuestionClick, item) -> {
                            if (isQuestionClick){

                            }else {
                                startActivity(new Intent(this, ForumComment.class));
                                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                            }

                        });
                        binding.rcvQuestion.setAdapter(adapterQuestion);

                    }
                }
            }
        }

        if(flag == ADDQUESTIONS){
            BaseResponseModel responseModel = (BaseResponseModel) object;
            if (responseModel.getStatus() == 200) {
                getQuestions(id);
                Utils.showToast(this,responseModel.getMessage());
            }
        }
    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
