package com.apps.uneleap.activity.profile.profile_tabs;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    String[] searchArray;
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm, String[] searchArray) {
        super(fm);
        mContext = context;
        this.searchArray = searchArray;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
       /* if (position == 0)
            return UserSearchFragment.newInstance(position);
        else if (position == 1)
            return SearchProtalksFragment.newInstance(position, searchArray[position]);
        else if (position == 2)
            return SearchCritiqueFragment.newInstance(position, searchArray[position]);
        else// if (position == 3)
            return SearchOtherFragment.newInstance(position, searchArray[position]);*/
        return null;

    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return searchArray[position];
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return searchArray.length;
    }


    /*
 sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(),
                    getResources().getStringArray(R.array.search_array));

        binding.viewPager.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewPager);
    * */
}