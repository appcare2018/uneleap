package com.apps.uneleap.activity.profile;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ProfileExpAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.EditEudAcBinding;
import com.apps.uneleap.model.profile.ProfileEducationModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.moos.library.Utils.dp2px;

public class EditEudAc extends AppCompatActivity {

    private EditEudAcBinding binding;
    CommonFunctions cmf;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.edit_eud_ac);
        binding.tvEditExp.setOnClickListener(v -> showSecondBottomDialog());
        initView();
    }


    private void initView() {
        cmf = new CommonFunctions(this);
        userId = cmf.myPreference.getString(this, USER_ID);
        //  userId="1";

        eud_loadData();
    }


    //--------------------------------------------------------
    ProfileEducationModel profileEducationModel;

    private void eud_loadData() {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getUserEducation(cmf.userID(userId)).enqueue(
                    new ApiCallBack<>(EditEudAc.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            profileEducationModel = (ProfileEducationModel) object;
                            if (profileEducationModel.getStatusCode() == 200) {
                                if (profileEducationModel.getData() != null) {
                                    eud_uiUpdate();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void eud_uiUpdate() {
        ProfileExpAdapter profileExpAdapter=new ProfileExpAdapter(profileEducationModel.getData(),R.layout.profile_experience_item,cmf);
        binding.rcvEudList.setAdapter(profileExpAdapter);
    }
    //--------------------------------------------------------

    private void showSecondBottomDialog() {
        Dialog bottomDialog = new Dialog(this, R.style.BottomDialog);
        View contentView = LayoutInflater.from(this).inflate(R.layout.bottom_curved_dialog, null);
        //--------------------
         LinearLayout myLayout = contentView.findViewById(R.id.llContainer);
         View itemInfo1 = contentView.inflate(this,R.layout.add_eudication_layout, myLayout);


        //--------------------
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - dp2px(this, 0f);
        // params.bottomMargin = dp2px(this, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //--------------------------------------------------------


}
