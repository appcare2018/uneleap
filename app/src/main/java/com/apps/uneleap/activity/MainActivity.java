package com.apps.uneleap.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.apps.uneleap.R;
import com.apps.uneleap.chat.Chat;
import com.apps.uneleap.databinding.ActivityMainBinding;
import com.apps.uneleap.fragement.CommunityPageFr;
import com.apps.uneleap.fragement.EventPageFr;
import com.apps.uneleap.fragement.ForumPageFr;
import com.apps.uneleap.fragement.HomePageFr;
import com.apps.uneleap.fragement.LibraryPageFr;
import com.apps.uneleap.fragement.NewsPost.NewsPageFr;
import com.apps.uneleap.util.CommonFunctions;


public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding viewDataBinding;
    private int selectedTab = 0;
    DrawerLayout drawer;
    private GestureDetectorCompat gestureDetectorCompat = null;
    private float x1, x2;
    static final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        bottomTab();
        topNavTab();


    }


    private void topNavTab() {
        viewDataBinding.topTab.profilePic.setOnClickListener(v -> {


            startActivity(new Intent(MainActivity.this, Tools.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

        });
        viewDataBinding.topTab.tabChat.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), Chat.class)));
        viewDataBinding.topTab.tabNotification.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), Notification.class)));
    }

    private void bottomTab() {


        viewDataBinding.bottomTab.IVBTab1.setOnClickListener(v -> clickedBottomTab(1));
        viewDataBinding.bottomTab.IVBTab2.setOnClickListener(v -> clickedBottomTab(2));
        viewDataBinding.bottomTab.IVBTab3.setOnClickListener(v -> clickedBottomTab(3));
        viewDataBinding.bottomTab.IVBTab4.setOnClickListener(v -> clickedBottomTab(4));
        viewDataBinding.bottomTab.IVBTab5.setOnClickListener(v -> clickedBottomTab(5));
        viewDataBinding.bottomTab.IVBTab6.setOnClickListener(v -> clickedBottomTab(6));
        clickedBottomTab(1);
    }

    private void clickedBottomTab(int i) {
        if (i == 1) {
            /*Clear stack*/
//            FragmentManager manager = getSupportFragmentManager();
//            for(int j = 0; j < manager.getBackStackEntryCount(); ++j)
//                manager.popBackStack();
            CommonFunctions.replaceFragment(MainActivity.this, new HomePageFr(), "");

        } else if (i == 2 && selectedTab != 2) {
            selectedTab = 2;
            CommonFunctions.replaceFragment(MainActivity.this, new NewsPageFr(), "");
        } else if (i == 3) {
            selectedTab = 3;
            CommonFunctions.replaceFragment(MainActivity.this, new ForumPageFr(), "");
        } else if (i == 4 && selectedTab != 4) {
            selectedTab = 4;
            CommonFunctions.replaceFragment(MainActivity.this, new CommunityPageFr(), "");
        } else if (i == 5 && selectedTab != 5) {
            selectedTab = 5;
            CommonFunctions.replaceFragment(MainActivity.this, new EventPageFr(), "");

        } else if (i == 6 && selectedTab != 6) {
            selectedTab = 6;
            CommonFunctions.replaceFragment(MainActivity.this, new LibraryPageFr(), "");
        }

        updateBottomTabColor(i);
    }

    public void updateBottomTabColor(int i) {
        selectedTab = i;
        viewDataBinding.bottomTab.IVBTab1.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);
        viewDataBinding.bottomTab.IVBTab2.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);
        viewDataBinding.bottomTab.IVBTab3.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);
        viewDataBinding.bottomTab.IVBTab4.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);
        viewDataBinding.bottomTab.IVBTab5.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);
        viewDataBinding.bottomTab.IVBTab6.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.GrayCloud), android.graphics.PorterDuff.Mode.SRC_ATOP);

        if (i == 1) {
            viewDataBinding.bottomTab.IVBTab1.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        } else if (i == 2) {
            viewDataBinding.bottomTab.IVBTab2.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        } else if (i == 3) {
            viewDataBinding.bottomTab.IVBTab3.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        } else if (i == 4) {
            viewDataBinding.bottomTab.IVBTab4.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        } else if (i == 5) {
            viewDataBinding.bottomTab.IVBTab5.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        } else if (i == 6) {
            viewDataBinding.bottomTab.IVBTab6.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_color), android.graphics.PorterDuff.Mode.SRC_ATOP);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        int count = manager.getBackStackEntryCount();
        if (count > 1)
            CommonFunctions.back_fragment();
        else
            finish();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;

                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // Left to Right swipe action
                    if (x2 > x1) {
                        // Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                    }

                    // Right to left swipe action
                    else {
                        // Toast.makeText(this, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                    }

                } else {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }
}
