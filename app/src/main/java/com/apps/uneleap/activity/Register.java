package com.apps.uneleap.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import com.apps.uneleap.R;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ActivityRegisterBinding;
import com.apps.uneleap.model.RegistrationModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.util.AppUtil.startActivityWithAnimation;

public class Register extends AppCompatActivity  implements ApiResponseErrorCallback {

    private ActivityRegisterBinding binding;
    String[] arrayGender ={"Male","Female"};
    ArrayAdapter<String> adapterGender;
    String userType="";
    private CommonFunctions cmf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        initView();
        onClickListners();
    }

    private void initView() {
        cmf = new CommonFunctions(this);
        selectUserType(2);

        adapterGender =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, arrayGender);

        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinnerGender.setAdapter(adapterGender);
    }

    private void selectUserType(int i) {
        userType="";
        binding.userTypeFaculty.setTypeface(Typeface.DEFAULT);
        binding.userTypeUniversity.setTypeface(Typeface.DEFAULT);
        binding.userTypeStudent.setTypeface(Typeface.DEFAULT);
        binding.userTypeAlumnus.setTypeface(Typeface.DEFAULT);

        binding.userTypeFaculty.setTextColor(getResources().getColor(R.color.black));
        binding.userTypeUniversity.setTextColor(getResources().getColor(R.color.black));
        binding.userTypeStudent.setTextColor(getResources().getColor(R.color.black));
        binding.userTypeAlumnus.setTextColor(getResources().getColor(R.color.black));
        binding.userTypeStudent.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius_gray));
        binding.userTypeUniversity.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius_gray));
        binding.userTypeFaculty.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius_gray));
        binding.userTypeAlumnus.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius_gray));
        if (i==1){
            userType="Student";
            binding.userTypeStudent.setTypeface(Typeface.DEFAULT_BOLD);
            binding.userTypeStudent.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius));
            binding.userTypeStudent.setTextColor(getResources().getColor(R.color.theme_color));
        }

        if (i==2){
            userType="University";
            binding.userTypeUniversity.setTypeface(Typeface.DEFAULT_BOLD);
            binding.userTypeUniversity.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius));
            binding.userTypeUniversity.setTextColor(getResources().getColor(R.color.theme_color));
        }

        if (i==3){
            userType="Faculty";
            binding.userTypeFaculty.setTypeface(Typeface.DEFAULT_BOLD);
            binding.userTypeFaculty.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius));
            binding.userTypeFaculty.setTextColor(getResources().getColor(R.color.theme_color));
        }

        if (i==4){
            userType="Alumnus";
            binding.userTypeAlumnus.setTypeface(Typeface.DEFAULT_BOLD);
            binding.userTypeAlumnus.setBackground(ContextCompat.getDrawable(this, R.drawable.button_shape_small_radius));
            binding.userTypeAlumnus.setTextColor(getResources().getColor(R.color.theme_color));
        }


    }

    private void onClickListners(){
        binding.userTypeStudent.setOnClickListener(v -> selectUserType(1));
        binding.userTypeUniversity.setOnClickListener(v -> selectUserType(2));
        binding.userTypeFaculty.setOnClickListener(v -> selectUserType(3));
        binding.userTypeAlumnus.setOnClickListener(v -> selectUserType(4));
    }


    // validating password with retype password
    private boolean isValidPassword(String val) {
        if (val != null && val.length() > 7) {
            return true;
        }
        return false;
    }

    private boolean isValidRePassword(String val) {
        if (val != null && val.length() > 0) {
            return true;
        }
        return false;
    }

    private boolean comparePassword(String password,String re_Password) {
        if (password.equals(re_Password)) {
            return true;
        }

        return false;
    }

    public void click_register(View view) {
        if (binding.etFirstName.getText().toString().isEmpty()){
            binding.etFirstName.requestFocus();
            binding.etFirstName.setError("Please Enter First Name");
        }
        else if (binding.etLastName.getText().toString().isEmpty()){
            binding.etLastName.requestFocus();
            binding.etLastName.setError("Please Enter Last Name");
        }
        else if (binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmail.requestFocus();
            binding.etEmail.setError("Please Enter Valid Email");
        }
        else if (!isValidPassword(binding.etEmail.getText().toString())) {
            binding.etEmail.requestFocus();
            binding.etEmail.setError("Please Enter Valid Email");
        }
        else if (binding.etPassword.getText().toString().isEmpty()) {
            binding.etPassword.requestFocus();
            binding.etPassword.setError("Please Enter Password");
        }
        else if (!comparePassword(binding.etPassword.getText().toString(),binding.etConfirmPassword.getText().toString())) {
            binding.etConfirmPassword.requestFocus();
            binding.etConfirmPassword.setError("Password is Not Matching");
        }
        else {
            sendToApi();
        }
    }

    private void sendToApi() {

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis()/1000;
            params.put("user_type", userType);
            params.put("username", binding.etEmail.getText().toString());
            params.put("first_name", binding.etFirstName.getText().toString());
            params.put("last_name", binding.etLastName.getText().toString());
            params.put("gender", binding.spinnerGender.getSelectedItem().toString());
            params.put("password", binding.etPassword.getText().toString());
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.registration(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, 1,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {

        RegistrationModel model = (RegistrationModel)object;
        if (model.getStatus_code()==200){

            Utils.showToast(this,model.getMessage()+"");
            startActivity( new Intent(getApplicationContext(),Login.class));
        }

        if (model.getStatus_code()==401){

            Utils.showToast(this,model.getMessage()+"");
            startActivity( new Intent(getApplicationContext(),Login.class));
        }

    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
