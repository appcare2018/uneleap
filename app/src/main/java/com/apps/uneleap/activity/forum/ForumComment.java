package com.apps.uneleap.activity.forum;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.CommentAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ForumCommentBinding;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.GetCommentByAnswerResponseModel;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.LIKEDISLIKE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.POSTCOMMENT;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.apps.uneleap.util.Utils.convertDate;

public class ForumComment extends AppCompatActivity implements ApiResponseErrorCallback {

    ForumCommentBinding binding;
    CommonFunctions cmf;
    QuestionResponseModel.Data model;
    private ArrayList<GetCommentByAnswerResponseModel.Data> commentDetails = new ArrayList<>();
    LinearLayoutManager questionLinearLayoutManager;
    private CommentAdapter adapterComment;
    String answerId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forum_comment);
        cmf = new CommonFunctions(this);

        questionLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.rcvComment.setLayoutManager(questionLinearLayoutManager);


        initView();


    }

    private void initView() {

        try {

            model = (QuestionResponseModel.Data) getIntent().getSerializableExtra("QuestionResponseModel");

            binding.tvQuestion.setText(model.getQuestion());
            if (model.getMost_like_answer().getAnswered_by() != null) {
                binding.tvName.setText(model.getMost_like_answer().getAnswered_by().getFirst_name() + " " +
                        model.getMost_like_answer().getAnswered_by().getLast_name());
            }

            if (model.getMost_like_answer().getAnswer() != null) {
                try {
                    Glide.with(this)
                            .load(model.getMost_like_answer().getAnswered_by().getImage_url())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .placeholder(R.drawable.profile_image)
                            .error(R.drawable.profile_image)
                            .into(binding.ivpc);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                binding.tvAnswer.setText(model.getMost_like_answer().getAnswer());
                binding.tvLike.setText(model.getMost_like_answer().getLikes());
                binding.tvComment.setText(model.getMost_like_answer().getComments());
                binding.tvAnsweredDate.setText("Answered " + convertDate(model.getMost_like_answer().getEdate(), "MMM dd yyyy"));

                if (model.getMost_like_answer().isLiked_by_you())
                    binding.ivLike.setChecked(true);
                answerId = model.getMost_like_answer().getAnswer_id();
                getQuestions(Integer.parseInt(answerId));

                binding.tvLike.setOnClickListener(view -> likeDislike(answerId, model.getMost_like_answer().getLikes()));

                binding.ivSend.setOnClickListener(view -> postComment(Integer.parseInt(model.getMost_like_answer().getAnswer_id()), binding.etComment.getText().toString()));
            }


        } catch (Exception e) {

        }
    }

    private void likeDislike(String answerId, String isLike) {

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis() / 1000;

            params.put("user_id", CommonFunctions.myPreference.getString(this, USER_ID));
            params.put("answer_id", answerId);
            params.put("like", isLike);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.likeDislikeAnswer(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            BaseResponseModel responseModel = (BaseResponseModel) object;
                            if (responseModel.getStatus() == 200) {
                                Utils.showToast(ForumComment.this, responseModel.getMessage());
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, LIKEDISLIKE, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }


    private void getQuestions(int answerId) {
        ///=---------------------------
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", CommonFunctions.myPreference.getString(this, USER_ID));
            params.put("answer_id", answerId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getCommentsByAnswerId(ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void postComment(int answer_id, String comment) {

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis();

            params.put("user_id", CommonFunctions.myPreference.getString(this, USER_ID));
            params.put("answer_id", answer_id);
            params.put("comment", comment);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.postCommentOfAnswer(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, POSTCOMMENT, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }


    @Override
    public void getApiResponse(Object object, int flag) {
        if (flag == QUESTIONS) {
            GetCommentByAnswerResponseModel responseModel = (GetCommentByAnswerResponseModel) object;
            if (responseModel.getStatus() == 200) {
                if (responseModel.getData() != null) {
                    if (responseModel.getData().size() != 0) {

                        commentDetails = new ArrayList<>();
                        commentDetails.addAll(responseModel.getData());

                        adapterComment = new CommentAdapter(this, commentDetails, (isQuestionClick, id, question) -> {


                        });
                        binding.rcvComment.setAdapter(adapterComment);

                    }
                }
            }
        }

        if (flag == POSTCOMMENT) {
            BaseResponseModel responseModel = (BaseResponseModel) object;
            if (responseModel.getStatus() == 200) {
                Utils.showToast(this, responseModel.getMessage());
                //super.onBackPressed();
                binding.etComment.getText().clear();
                getQuestions(Integer.parseInt(answerId));
            }
        }
    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
