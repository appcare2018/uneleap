package com.apps.uneleap.activity.profile;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.ProfileExpAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.controller.Controller;
import com.apps.uneleap.databinding.EditExpAcBinding;
import com.apps.uneleap.model.profile.ProfileExperienceModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.DateTime_c;
import com.apps.uneleap.util.Utils;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.moos.library.Utils.dp2px;

public class EditExpActivity extends AppCompatActivity {

    private EditExpAcBinding binding;
    CommonFunctions cmf;
    String userId;
    public DateTime_c dateTime_c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.edit_exp_ac);

        initView();
    }


    private void initView() {
        dateTime_c=new DateTime_c();
        cmf = new CommonFunctions(this);
        userId = CommonFunctions.myPreference.getString(this, USER_ID);
       //   userId="1";
        binding.tvEditExp.setOnClickListener(v -> showSecondBottomDialog());
        exp_loadData();
    }

    //--------------------------------------------------------
    ProfileExperienceModel profileExperienceModel;

    private void exp_loadData() {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getUserExperience(cmf.userID(userId)).enqueue(
                    new ApiCallBack<>(EditExpActivity.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {

                            profileExperienceModel = (ProfileExperienceModel) object;
                            if (profileExperienceModel.getStatusCode() == 200) {
                                if (profileExperienceModel.getData() != null) {
                                    exp_uiUpdate();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    private void exp_uiUpdate() {
        ProfileExpAdapter profileExpAdapter = new ProfileExpAdapter(this, profileExperienceModel.getData(), R.layout.profile_experience_item, cmf);
        binding.rcvExpList.setAdapter(profileExpAdapter);
    }

    //--------------------------------------------------------

    private void showSecondBottomDialog() {
        Dialog bottomDialog = new Dialog(this, R.style.BottomDialog);
        View contentView = LayoutInflater.from(this).inflate(R.layout.bottom_curved_dialog, null);
        //--------------------
        LinearLayout myLayout = contentView.findViewById(R.id.llContainer);
        View itemInfo1 = contentView.inflate(this,R.layout.add_exp_layout, myLayout);

        //--------------------
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - dp2px(this, 0f);

        //-----------------------------------------------
        TextView start=itemInfo1.findViewById(R.id.etStart);
        TextView end=itemInfo1.findViewById(R.id.etEnd);
        Button btAdd=itemInfo1.findViewById(R.id.btAdd);
        start.setOnClickListener(v->new DateTime_c().datePickerDialog(bottomDialog.getContext(), new Controller() {
            @Override
            public void callback(String result) {
                start.setText(result);
            }
        }));end.setOnClickListener(v->new DateTime_c().datePickerDialog(bottomDialog.getContext(), new Controller() {
            @Override
            public void callback(String result) {
                end.setText(result);
            }
        }));
        btAdd.setOnClickListener(v->addData("om","test","test","test","test","test"));



        //-----------------------------------------------



       // params.bottomMargin = dp2px(this, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    private void addData(String s, String s1, String s2, String s3, String s4, String s5) {
        //userId="1";
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.postExperience(cmf.postExperience(userId,s,s1,s2,s3,s4,s5,"")).enqueue(
                    new ApiCallBack<>(EditExpActivity.this, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {

                            profileExperienceModel = (ProfileExperienceModel) object;
                            if (profileExperienceModel.getStatusCode() == 200) {
                                if (profileExperienceModel.getData() != null) {
                                    exp_loadData();
                                }
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, QUESTIONS, true)
            );
        } else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    //--------------------------------------------------------


}
