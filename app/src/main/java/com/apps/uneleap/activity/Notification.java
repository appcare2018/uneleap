package com.apps.uneleap.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.NotificationAdapter;
import com.apps.uneleap.databinding.ActivityNotificationBinding;
import com.apps.uneleap.model.NotificationChatModel;
import com.apps.uneleap.model.NotificationForumModel;
import com.apps.uneleap.model.NotificationNewsModel;
import com.apps.uneleap.model.NotificationTitleModel;

import java.util.ArrayList;

public class Notification extends AppCompatActivity {

    ArrayList<Object> notifications = new ArrayList<>();
    ArrayList<NotificationChatModel> notificationChatModels = new ArrayList<>();
    ActivityNotificationBinding activityNotificationBinding;
    NotificationAdapter notificationAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_notification);

        activityNotificationBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        addItemsInChatModel();
        AddItems();
        activityNotificationBinding.notificationRV.setLayoutManager(new LinearLayoutManager(this));
        notificationAdapter = new NotificationAdapter(this, notifications);
        activityNotificationBinding.notificationRV.setAdapter(notificationAdapter
        );
    }


    private void AddItems() {
        notifications.add(new NotificationTitleModel(1, "Latest News"));
        notifications.add((new NotificationNewsModel(1, "An Evening With Author Scott Pelley", "Oct 6,2018")));
        notifications.add((new NotificationNewsModel(1, "An Evening With Author Scott Pelley", "Oct 6,2018")));
        notifications.add((new NotificationNewsModel(1, "An Evening With Author Scott Pelley", "Oct 6,2018")));
        notifications.add(new NotificationTitleModel(1, "Forum"));
        notifications.add(new NotificationForumModel(1, "Ethan Hunter", "Answered Your Question"));
        notifications.add(new NotificationForumModel(1, "Ethan Hunter", "Answered Your Question"));
        notifications.add(new NotificationForumModel(1, "Ethan Hunter", "Answered Your Question"));
        notifications.add(new NotificationTitleModel(1, "Chat"));
        notifications.add(notificationChatModels);
    }

    private void addItemsInChatModel() {
        notificationChatModels.add(new NotificationChatModel(1, 1));
        notificationChatModels.add(new NotificationChatModel(1, 1));
        notificationChatModels.add(new NotificationChatModel(1, 1));
        notificationChatModels.add(new NotificationChatModel(1, 1));
        notificationChatModels.add(new NotificationChatModel(1, 1));
        notificationChatModels.add(new NotificationChatModel(1, 1));
    }
}
