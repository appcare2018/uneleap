package com.apps.uneleap.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.apps.uneleap.R;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ActivityLoginBinding;
import com.apps.uneleap.databinding.ActivityRegisterBinding;
import com.apps.uneleap.model.LoginResponseModel;
import com.apps.uneleap.model.RegistrationModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.util.AppUtil.startActivityWithAnimation;
import static com.apps.uneleap.util.GlobalConstants.IS_LOGIN;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;

public class Login extends AppCompatActivity implements ApiResponseErrorCallback {

    private ActivityLoginBinding binding;
    private CommonFunctions cmf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams attrib = getWindow().getAttributes();
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        cmf = new CommonFunctions(this);
      //  CommonFunctions.myPreference.setBoolean(this, IS_LOGIN, true);
    }



    public void on_click(View view) {
         if (binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmail.requestFocus();
            binding.etEmail.setError("Please Enter Valid Email");
        }
         else if (binding.etPassword.getText().toString().isEmpty()) {
            binding.etPassword.requestFocus();
            binding.etPassword.setError("Please Enter Password");
        }
        else
            sendToApi();

    }

    public void forgot_password(View view) {
        startActivity( new Intent(getApplicationContext(),ForgotPassword.class));
    }


    private void sendToApi() {

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis()/1000;

            params.put("username", binding.etEmail.getText().toString());
            params.put("password", binding.etPassword.getText().toString());
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.login(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(this, this, 1,true)
            );
        }else {
            Utils.showToast(this, getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {

        LoginResponseModel model = (LoginResponseModel)object;
        if (model.getStatus()==200){
            CommonFunctions.myPreference.setString(this, USER_ID, model.getData().getUser_id());
            CommonFunctions.myPreference.setBoolean(this, IS_LOGIN, true);
            startActivity( new Intent(getApplicationContext(),MainActivity.class));
        }
        else {
            Utils.showToast(this,model.getMessage());

        }

    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
