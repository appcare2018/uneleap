package com.apps.uneleap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GetCommentByAnswerResponseModel extends BaseResponseModel{


    @SerializedName("data")
    @Expose
    private ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable {



        @SerializedName("answer_comment_id")
        @Expose
        private String answer_comment_id;

        @SerializedName("comment")
        @Expose
        private String comment;

        @SerializedName("likes")
        @Expose
        private String likes;

        @SerializedName("is_like")
        @Expose
        private int is_like;

        public int getIs_like() {
            return is_like;
        }

        public void setIs_like(int is_like) {
            this.is_like = is_like;
        }

        public boolean isLiked_by_you() {
            return liked_by_you;
        }

        public void setLiked_by_you(boolean liked_by_you) {
            this.liked_by_you = liked_by_you;
        }

        @SerializedName("liked_by_you")
        @Expose
        private boolean liked_by_you;

        @SerializedName("comment_by")
        @Expose
        private Comment_by comment_by;



        public String getAnswer_comment_id() {
            return answer_comment_id;
        }

        public void setAnswer_comment_id(String answer_comment_id) {
            this.answer_comment_id = answer_comment_id;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }


        public String getLikes() {
            return likes;
        }

        public void setLikes(String likes) {
            this.likes = likes;
        }



        public Comment_by getComment_by() {
            return comment_by;
        }

        public void setComment_by(Comment_by answered_by) {
            this.comment_by = answered_by;
        }




    }

    public class Comment_by implements Serializable{

        @SerializedName("first_name")
        @Expose
        private String first_name;

        @SerializedName("last_name")
        @Expose
        private String last_name;

        @SerializedName("designation")
        @Expose
        private String designation;

        @SerializedName("image_url")
        @Expose
        private String image_url;

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }


    }



}
