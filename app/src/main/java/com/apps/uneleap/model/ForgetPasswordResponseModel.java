package com.apps.uneleap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Gaurav on 16-05-2019.
 */

public class ForgetPasswordResponseModel {

    @Expose
    @SerializedName("status_code")
    private Integer status;

    @Expose
    @SerializedName("message")
    private String message;

    @SerializedName("temporary_password")
    @Expose
    private String temporary_password;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTemporary_password() {
        return temporary_password;
    }

    public void setTemporary_password(String temporary_password) {
        this.temporary_password = temporary_password;
    }





}
