package com.apps.uneleap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetQuestionResponseModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    //--------------------------------------------------------

    public class MostLikeAnswer {

        @SerializedName("answer_id")
        @Expose
        private String answerId;
        @SerializedName("answer")
        @Expose
        private String answer;
        @SerializedName("edate")
        @Expose
        private String edate;
        @SerializedName("likes")
        @Expose
        private String likes;
        @SerializedName("comments")
        @Expose
        private Integer comments;
        @SerializedName("answered_by")
        @Expose
        private AnsweredBy answeredBy;

        public String getAnswerId() {
            return answerId;
        }

        public void setAnswerId(String answerId) {
            this.answerId = answerId;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getEdate() {
            return edate;
        }

        public void setEdate(String edate) {
            this.edate = edate;
        }

        public String getLikes() {
            return likes;
        }

        public void setLikes(String likes) {
            this.likes = likes;
        }

        public Integer getComments() {
            return comments;
        }

        public void setComments(Integer comments) {
            this.comments = comments;
        }

        public AnsweredBy getAnsweredBy() {
            return answeredBy;
        }

        public void setAnsweredBy(AnsweredBy answeredBy) {
            this.answeredBy = answeredBy;
        }

    }

    //---------------------------------------------------------
    public class Datum {

        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("question")
        @Expose
        private String question;
        @SerializedName("topic")
        @Expose
        private String topic;
        @SerializedName("most_like_answer")
        @Expose
        private MostLikeAnswer mostLikeAnswer;

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public MostLikeAnswer getMostLikeAnswer() {
            return mostLikeAnswer;
        }

        public void setMostLikeAnswer(MostLikeAnswer mostLikeAnswer) {
            this.mostLikeAnswer = mostLikeAnswer;
        }

    }



    //-----------------------------------------------------------
    public class AnsweredBy {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("image_url")
        @Expose
        private String imageUrl;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

    }




}
