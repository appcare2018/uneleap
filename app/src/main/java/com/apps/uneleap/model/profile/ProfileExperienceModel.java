package com.apps.uneleap.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileExperienceModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    //-------------------------------------------
    public class Datum {

        @SerializedName("user_experience_id")
        @Expose
        private String userExperienceId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("university")
        @Expose
        private String university;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("is_currently_working")
        @Expose
        private String isCurrentlyWorking;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("edate")
        @Expose
        private String edate;

        public String getUserExperienceId() {
            return userExperienceId;
        }

        public void setUserExperienceId(String userExperienceId) {
            this.userExperienceId = userExperienceId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUniversity() {
            return university;
        }

        public void setUniversity(String university) {
            this.university = university;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getIsCurrentlyWorking() {
            return isCurrentlyWorking;
        }

        public void setIsCurrentlyWorking(String isCurrentlyWorking) {
            this.isCurrentlyWorking = isCurrentlyWorking;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getEdate() {
            return edate;
        }

        public void setEdate(String edate) {
            this.edate = edate;
        }

    }
    //-------------------------------------------
}
