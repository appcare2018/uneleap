package com.apps.uneleap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class QuestionResponseModel extends BaseResponseModel{


    @SerializedName("data")
    @Expose
    private ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable {

        @SerializedName("question_id")
        @Expose
        private String question_id;

        @SerializedName("question")
        @Expose
        private String question;

        @SerializedName("topic")
        @Expose
        private String topic;



        @SerializedName("most_like_answer")
        @Expose
        private  MostLikeAnswer most_like_answer;


        public String getQuestion_id() {
            return question_id;
        }

        public void setQuestion_id(String question_id) {
            this.question_id = question_id;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public  MostLikeAnswer getMost_like_answer() {
            return most_like_answer;
        }

        public void setMost_like_answer( MostLikeAnswer most_like_answer) {
            this.most_like_answer = most_like_answer;
        }






    }

    public class  MostLikeAnswer implements Serializable{

        @SerializedName("answer_id")
        @Expose
        private String answer_id;

        @SerializedName("answer")
        @Expose
        private String answer;

        @SerializedName("edate")
        @Expose
        private String edate;

        @SerializedName("likes")
        @Expose
        private String likes;

        @SerializedName("comments")
        @Expose
        private String comments;

            @SerializedName("answered_by")
            @Expose
            private AnsweredBy answered_by;

        public String getAnswer_id() {
            return answer_id;
        }


        public boolean isLiked_by_you() {
            return liked_by_you;
        }

        public void setLiked_by_you(boolean liked_by_you) {
            this.liked_by_you = liked_by_you;
        }

        @SerializedName("liked_by_you")
        @Expose
        private boolean liked_by_you;



        public void setAnswer_id(String answer_id) {
            this.answer_id = answer_id;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getEdate() {
            return edate;
        }

        public void setEdate(String edate) {
            this.edate = edate;
        }

        public String getLikes() {
            return likes;
        }

        public void setLikes(String likes) {
            this.likes = likes;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

            public AnsweredBy getAnswered_by() {
                return answered_by;
            }

            public void setAnswered_by(AnsweredBy answered_by) {
                this.answered_by = answered_by;
            }


        public class AnsweredBy implements Serializable{

            @SerializedName("first_name")
            @Expose
            private String first_name;

            @SerializedName("last_name")
            @Expose
            private String last_name;

            @SerializedName("designation")
            @Expose
            private String designation;

            @SerializedName("image_url")
            @Expose
            private String image_url;

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getDesignation() {
                return designation;
            }

            public void setDesignation(String designation) {
                this.designation = designation;
            }

            public String getImage_url() {
                return image_url;
            }

            public void setImage_url(String image_url) {
                this.image_url = image_url;
            }


        }

    }



}
