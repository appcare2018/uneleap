package com.apps.uneleap.model;

public class NotificationChatModel {

    private int resource;
    private int status;


    public NotificationChatModel(int resource, int status) {
        this.resource = resource;
        this.status = status;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
