package com.apps.uneleap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponseModel {

    @Expose
    @SerializedName("status_code")
    private Integer status;

    @Expose
    @SerializedName("status_message")
    private String message;

//    @Expose
//    @SerializedName("error")
//    private Error error;
//
//    @Expose
//    @SerializedName("time")
//    private long time;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public Error getError() {
//        return error;
//    }
//
//    public void setError(Error error) {
//        this.error = error;
//    }
//
//    public long getTime() {
//        return time;
//    }
//
//    public void setTime(long time) {
//        this.time = time;
//    }

//    public class Error {
//
//        @SerializedName("errorCode")
//        @Expose
//        private int errorCode;
//        @SerializedName("message")
//        @Expose
//        private String message;
//
//        public int getErrorCode() {
//            return errorCode;
//        }
//
//        public void setErrorCode(int errorCode) {
//            this.errorCode = errorCode;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//    }
}
