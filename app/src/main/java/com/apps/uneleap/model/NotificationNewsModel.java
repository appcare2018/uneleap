package com.apps.uneleap.model;

public class NotificationNewsModel {

    private int resource;
    private String title;
    private String date;

    public NotificationNewsModel(int resource, String title, String date) {
        this.resource = resource;
        this.title = title;
        this.date = date;
    }


    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
