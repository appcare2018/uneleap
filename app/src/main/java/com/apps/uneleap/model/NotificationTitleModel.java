package com.apps.uneleap.model;

public class NotificationTitleModel {

    private int resource;
    private String title;


    public NotificationTitleModel(int resource, String title) {
        this.resource = resource;
        this.title = title;

    }


    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
