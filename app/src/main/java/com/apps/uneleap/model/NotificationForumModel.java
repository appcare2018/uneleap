package com.apps.uneleap.model;

public class NotificationForumModel {

    private int resource;
    private String username;
    private String message;


    public NotificationForumModel(int resource, String username, String message) {
        this.resource = resource;
        this.username = username;
        this.message = message;
    }


    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
