package com.apps.uneleap.model;

public class AllTopicSpinnerModel {
    private String id;
    private String value;

    public AllTopicSpinnerModel() {

    }

    public AllTopicSpinnerModel(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Pay attention here, you have to override the toString method as the
     * ArrayAdapter will reads the toString of the given object for the name
     *
     * @return contact_name
     */
    @Override
    public String toString() {
        return value;
    }

}
