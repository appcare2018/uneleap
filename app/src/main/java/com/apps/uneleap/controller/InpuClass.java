package com.apps.uneleap.controller;

import java.util.HashMap;

public class InpuClass {

    public   HashMap<String, Object> getFAq(String userId, String page) {

        final HashMap<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("page", page);

        return params;
    }
     public   HashMap<String, Object> userID(String userId) {

        final HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", userId);


        return params;
    }

    public   HashMap<String, Object> postExperience(String userId,String title,
                                            String university,String start_date,
                                            String end_date,String is_currently_working
            ,String location,String edate) {

        final HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", userId);
        params.put("title", title);
        params.put("university", university);
        params.put("start_date", start_date);//in milisec
        params.put("end_date", end_date);
        params.put("is_currently_working", is_currently_working);// "":0/1, 0 for no / 1 for yes
        params.put("location", location);
        params.put("edate", edate);


        return params;
    }


}
