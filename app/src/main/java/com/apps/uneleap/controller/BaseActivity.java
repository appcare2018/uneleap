package com.apps.uneleap.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.apps.uneleap.R;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Controller;
import com.google.android.material.snackbar.Snackbar;


public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    protected ViewDataBinding viewBaseBinding;
    ProgressBar progressBar;
    public CommonFunctions cmf;
    public TextView titleTv, actionTv, tv_side_two;
    public ImageView ivHead, mHeaderAction, ivLeftOne, ivbackimage, ivSide_one, ivSide_two;
    protected TextView mSkipButton;
    RelativeLayout rlback_toolbar;
    public Controller controller;
    private ProgressDialog dialog;


    @Override
    protected void attachBaseContext(Context base) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cmf = new CommonFunctions(this);
        viewBaseBinding = DataBindingUtil.setContentView(this, getLayoutById());

        if (viewBaseBinding != null) {
            /*if (viewBaseBinding instanceof ActivitySplashBinding || viewBaseBinding instanceof ActivityWelcomeBinding
                    || viewBaseBinding instanceof ActivityGetStartedBinding) {
                *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(Color.parseColor(get));
                }*//*
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window w = getWindow();
                    w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
                }
               *//* if (this instanceof LoginOptionAc) {
                    cmf.clearAllOffline(getActivity());
                }*//*
            }*/
        }

        /// mMaterialProgressBar = findViewById(R.id.circular_progress_bar);
        /*rlback_toolbar = findViewById(R.id.rl_back_toolbar);
        titleTv = findViewById(R.id.tv_title);
        ivHead = findViewById(R.id.ivHead);
        ivSide_one = findViewById(R.id.iv_side_one);
        ivSide_two = findViewById(R.id.iv_side_two);
        tv_side_two = findViewById(R.id.tv_side_two);
        ivbackimage = findViewById(R.id.ivLeftOne);*/
        // progressBar = (ProgressBar) findViewById(R.id.imgprogress);
        if (ivbackimage != null) {
            ivbackimage.setOnClickListener(v -> onHeaderLeftAction1Clicked());
        }

        initUi();
    }


    public void ivLeftOneGo(View v) {
        //cmf.startAc(NotificationActivity.class);
    }


    protected void setmSkipButtonGone() {
        mSkipButton.setVisibility(View.GONE);
    }


    protected void onHeaderLeftAction1Clicked() {
        finish();
    }

    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }


    protected void setRightActionVisibility(int visibility) {
        if (mHeaderAction != null) {
            mHeaderAction.setVisibility(visibility);
        }
    }

    protected void sideTwo(int resource, Class classs) {
        if (ivSide_two != null) {
            ivSide_two.setImageResource(resource);
            ivSide_two.setVisibility(View.VISIBLE);
           /* if (classs != null)
                ivSide_two.setOnClickListener(view1 -> cmf.startAc(getActivity(), classs));*/
        }
    }

    protected void setActionTvText(String text) {
        if (actionTv != null) {
            actionTv.setText(text);
        }
    }

    protected String getActionTvText() {
        if (actionTv != null) {
            return actionTv.getText().toString();
        }
        return "";
    }


    protected void setHeaderTitle(String title) {
        if (titleTv != null) {
            titleTv.setText(title);
            hideTitleImage();
        }
    }

    protected void titleBackVisibility(int visibility) {
        if (ivbackimage != null) {
            ivbackimage.setVisibility(visibility);
        }
    }

    protected void hideTitleImage() {
        if (ivHead != null) {
            ivHead.setVisibility(View.GONE);
        }
    }


    protected String getUserId() {
        return cmf.myPreference.getString(getActivity(), cmf.gc.USER_ID);
    }

    protected BaseActivity getActivity() {
        return this;
    }

 /*   protected void onHeaderBackClicked() {
        try {
            Utility.hideKeyboard(getActivity());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        onBackPressed();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Initialize ui parameters after layout inflation
     */
    protected abstract void initUi();

    protected abstract int getLayoutById();


   /* public void showProgressDialog(boolean iShow) {
        if (mMaterialProgressDialog == null) {
            mMaterialProgressDialog = Utility.getProgressDialogInstance(this);
        }
        try {
            if (mMaterialProgressDialog != null) {
                if (iShow) {
                    mMaterialProgressDialog.show();
                } else {
                    mMaterialProgressDialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public void showProgressBar(boolean iShow) {
        if (progressBar != null) {
            if (iShow) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // cmf.lastActivityContext = getActivity();

        //  register();
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void setControllerListner(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("-------", "onConfigurationChanged");
    }

    public void createProgressDialog(Context context) {
        dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException ignored) {
        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progress_dialog);
    }

    public void dismissDialog() {
        if (!BaseActivity.this.isFinishing()) {
            if (dialog != null)
                dialog.dismiss();
        }
    }


    private void showSnake(String result) {
        final View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = null;
        snackbar.make(parentLayout, result, Snackbar.LENGTH_LONG)
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();

    }


}