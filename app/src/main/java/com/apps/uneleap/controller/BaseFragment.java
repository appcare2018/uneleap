package com.apps.uneleap.controller;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;


import com.apps.uneleap.util.CommonFunctions;
import com.google.android.material.snackbar.Snackbar;

/**
 * Base Fragment class for all fragments
 */
public abstract class BaseFragment extends Fragment   {

    protected View view;
    protected Context mContext;

    public TextView titleTv, tvLeftOne;
    public ImageView ivLeftOne, ivHead, ivSide_one, ivSide_two;
    RelativeLayout rlback_toolbar;
    LinearLayout llProgress;
    EditText etSearch;

    protected Snackbar mSnackBar;
    public CommonFunctions cmf;
    protected ViewDataBinding viewDataBinding;

    @Override
    public void onDetach() {
        super.onDetach();
        if (mSnackBar != null) {
            mSnackBar.dismiss();
        }
        viewDataBinding = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        cmf = new CommonFunctions(mContext);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);
            if (viewDataBinding != null) {
                return viewDataBinding.getRoot();
            } else {
                return inflater.inflate(getLayoutById(), container, false);
            }
            //-------------
        } catch (Exception e) {
            e.printStackTrace();
            return inflater.inflate(getLayoutById(), container, false);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        //  mMaterialProgressBar = (CircularProgressView) view.findViewById(R.id.circular_progress_bar);



        initUi();
    }

    /**
     * Method to find view by id, just like activities findViewById
     *
     * @param resId Resource id to fine
     * @return View instance of res id
     */
    protected View findViewById(int resId) {
        return view.findViewById(resId);
    }

    /**
     * Initialize ui parameters after layout inflation
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     *
     * @return Layout res id
     */
    protected abstract int getLayoutById();

    protected String getUserId() {
        return cmf.myPreference.getString(mContext, cmf.gc.USER_ID);
    }


  /*  public void showProgressDialog(boolean iShow) {
        if (mMaterialProgressDialog == null) {
            if (mContext != null) {
                mMaterialProgressDialog = Utility.getProgressDialogInstance(mContext);
            }
        }
        try {
            if (mMaterialProgressDialog != null) {
                if (iShow) {
                    mMaterialProgressDialog.show();
                } else {
                    mMaterialProgressDialog.dismiss();
                }
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }*/


    protected void setHeaderTitle(String title) {
        if (titleTv != null) {
            titleTv.setText(title);
            hideTitleImage();
        }
    }

    protected void progressVisibility(int visibility) {
        if (llProgress != null) {
            llProgress.setVisibility(visibility);

            /*else
            //  cmf.animate(getActivity(), llProgress, R.anim.popup_hide);*/

        }
    }


    protected void hideTitleImage() {
        if (ivHead != null) {
            ivHead.setVisibility(View.GONE);
        }
    }


    protected void showToast(String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

}