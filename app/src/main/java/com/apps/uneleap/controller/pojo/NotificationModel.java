package com.apps.uneleap.controller.pojo;

import com.apps.uneleap.controller.viewModel.NotificationViewModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NotificationModel extends NotificationViewModel {


    @SerializedName("result")
    @Expose
    public List<Notifications> result = null;

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("nextPage")
    @Expose
    public Boolean nextPage;

    @SerializedName("message")
    @Expose
    public String message;

    public class Notifications extends NotificationViewModel {

        @SerializedName("_id")
        @Expose
        public String id;

        @SerializedName("message")
        @Expose
        public String message;

        @SerializedName("profileImage")
        @Expose
        public String profileImage;

        @SerializedName("created_date")
        @Expose
        public String createdAt;

        @SerializedName("updatedAt")
        @Expose
        public String updatedAt;

        public String getTime() {
            return dateFormat(createdAt);
        }

        private String dateFormat(String dateS) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date date = null;
            try {
                date = dateFormat.parse(dateS);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat formatter = new SimpleDateFormat("HH:mm dd MMM yyyy", Locale.US);
            return formatter.format(date);
        }

    }
}
