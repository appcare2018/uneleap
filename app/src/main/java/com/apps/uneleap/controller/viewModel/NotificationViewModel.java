package com.apps.uneleap.controller.viewModel;

import androidx.databinding.BaseObservable;

import com.apps.uneleap.R;
import com.apps.uneleap.controller.callBack.ViewModel;

/**
 * Created by Om on 17-01-2020.
 */
public class NotificationViewModel extends BaseObservable implements ViewModel {

    @Override
    public int layoutId(int position) {
        return R.layout.notification_item;
    }
}
