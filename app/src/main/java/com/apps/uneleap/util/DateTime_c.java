package com.apps.uneleap.util;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;

import com.apps.uneleap.controller.Controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by omprakash.m on 3/9/2018.
 */

public class DateTime_c {

    public static DateTime_c instance;
    SimpleDateFormat df;

    public DateTime_c() {
    }

    public static DateTime_c getInstance() {
        if (instance == null) {
            instance = new DateTime_c();
        }
        return instance;
    }

    public String currentTimeAmPm() {
        String returnDtm = "";
        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            returnDtm = new SimpleDateFormat("hh:mm a").format(calendar.getTime());
        } catch (Exception ex) {
        }
        return returnDtm;
    }

    public String amPm_to_24min(String ampm) {
        String returnDtm = "";
        try {
            df= new SimpleDateFormat("hh:mm a");
            Date date = df.parse(ampm);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int hours = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);
            System.out.println("------time-:" + (hours*60)+min);

            returnDtm = String.valueOf((hours*60)+min);
        } catch (Exception ex) {
        }
        return returnDtm;
    }


    public String currentDateTime(String new_df) {
        String returnDtm = "";
        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            //returnDtm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
            returnDtm = new SimpleDateFormat(new_df).format(calendar.getTime());
        } catch (Exception ex) {
        }

        return returnDtm;
    }

    public String dateTimeDifference(String new_df, String startDateTm, String endDateTm, String diffIn) {
        String Difference = "";
        try {
            //df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df = new SimpleDateFormat(new_df);
            Date startDate = df.parse(startDateTm);
            Date endDate = df.parse(endDateTm);

            //milliseconds
            long different = endDate.getTime() - startDate.getTime();

            System.out.println("startDate : " + startDate);
            System.out.println("endDate : " + endDate);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;


            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            if (diffIn.equals("days")) {
                Difference = String.valueOf(elapsedDays);
            } else if (diffIn.equals("hours")) {
                Difference = String.valueOf(elapsedHours);
            } else if (diffIn.equals("minutes")) {
                Difference = String.valueOf(elapsedMinutes);
            } else if (diffIn.equals("seconds")) {
                Difference = String.valueOf(elapsedSeconds);
            } else {
                NumberFormat formatter = new DecimalFormat("00");
                Difference = formatter.format(elapsedDays) + ":" + formatter.format(elapsedHours) + ":" + formatter.format(elapsedMinutes);
                System.out.printf("----->%d days, %d hours, %d minutes, %d seconds%n",
                        elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
            }
            // outPut..3 days, 9 hours, 5 minutes, 45 seconds
        } catch (Exception ex) {
        }
        return Difference;
    }

    public String formateDateTime(String parseDateTime) {
        String returnDtm = "";
        try {

            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            final String C_Year = String.valueOf(calendar.get(Calendar.YEAR));
            final String C_Day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            final String C_Month = new SimpleDateFormat("MMM").format(calendar.getTime());

            //---------------------------------------------
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse(parseDateTime);
            //for server date
            calendar.setTime(date);

            final String Year = String.valueOf(calendar.get(Calendar.YEAR));
            final String Day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            final String Month = new SimpleDateFormat("MMM").format(calendar.getTime());
            final String Time = new SimpleDateFormat("hh:mm a").format(date);

            if (C_Year.equals(Year)) {
                if (C_Month.equals(Month) && C_Day.equals(Day)) {
                    returnDtm = Time;
                } else {
                    returnDtm = Day + " " + Month;
                }
            } else {
                returnDtm = parseDateTime;
            }

            //---------------------------------------------

        } catch (Exception ex) {
        }

        return returnDtm;
    }

    public String changeFormate(String parseDateTime) {
        String returnDtm = "";
        try {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse(parseDateTime);

            returnDtm = new SimpleDateFormat("d MMM yyyy  h:mm a").format(date);

        } catch (Exception ex) {
        }
        return returnDtm;
    }

    public boolean dateExistInFromToDate(String from, String to, String compare) {
        boolean status = false;
        try {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Date fromD = df.parse(from);
            final Date toD = df.parse(to);
            final Date compareDate = df.parse(compare);

            if (compareDate.after(fromD) && compareDate.before(toD)) {
                //date >0 will exist in from date  AND
                Log.d("---after-->", "date >0 will exist in from date" + compare);
                status = true;
            }
           /* if (compareDate.before(toD)) {
                //date <0 will exist in to date
                Log.d("---before-->", "date <0 will exist in to date "+compare);
            }*/
        } catch (Exception Ex) {
        }
        return status;
    }

    public boolean isDateAftr(String date_s, String date_e) {
        boolean status = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            final Date strDate = sdf.parse(date_s);
            final Date endDate = sdf.parse(date_e);
            if (endDate.getTime() > strDate.getTime() || endDate.getTime() == strDate.getTime()) {
                status = true;
            }

        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
        return status;
    }

    public void datePickerDialog(final Context context, final Controller controller) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String dd = String.valueOf(dayOfMonth);
                    String mm = String.valueOf((monthOfYear + 1));
                    if (dayOfMonth < 10) {
                        dd = "0" + dd;
                    }
                    if ((monthOfYear + 1) < 10) {
                        mm = "0" + mm;
                    }
                    controller.callback(mm + "/" + dd + "/" + year);

                    // txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                }, mYear, mMonth, mDay);
        //  datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);// can not select previous date
        datePickerDialog.show();
    }
}
