package com.apps.uneleap.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.apps.uneleap.activity.GetStarted;
import com.apps.uneleap.activity.Login;
import com.apps.uneleap.viewpager.ViewPagerActivity;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import static com.apps.uneleap.util.AppUtil.startActivityWithAnimation;

/**
 * Created by Gaurav on 16-05-2019.
 */
public class Utils {

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void pagerCount(int count) {

    }

    public static void logoutUser(Activity context){
        try{
            CommonFunctions.myPreference.clearSharedPreference(context);
            Intent intent = new Intent(context, GetStarted.class);
            startActivityWithAnimation(context, intent);
            context.finishAffinity();
        }catch (Exception ex){
           ex.printStackTrace();
        }

    }

    public static void hideKeyBoard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE));
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void hideKeyboardFromFragment(Context context, Fragment fragment) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(Objects.requireNonNull(fragment.getView()).getRootView().getWindowToken(), 0);
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void shareBitmap(Context context, Bitmap bitmap, String fileName, String text) {

       try {
            File file = File.createTempFile(fileName, ".png", context.getExternalCacheDir());
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);

            Intent emailIntent = new Intent();
            emailIntent.setAction(Intent.ACTION_SEND);
           emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Alpha Match");
           emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            emailIntent.putExtra(Intent.EXTRA_TEXT, text);
            emailIntent.setType("image/png");
            context.startActivity(Intent.createChooser(emailIntent, "Share..."));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void shareImageWithText(Context context, String title, String text, Bitmap image) {
        Uri imageUri = null;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), image, "Title", null);
            imageUri = Uri.parse(path);

        } catch (Exception ignored) {}

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        share.putExtra(Intent.EXTRA_STREAM, "https://www.facebook.com/sharer/sharer.php?u="+text);
        share.putExtra(Intent.EXTRA_SUBJECT, text);
        share.putExtra(Intent.EXTRA_TEXT, "http://stackoverflow.com/questions/7545254");

       // context.startActivity(Intent.createChooser(share, "Share..."));

        ((Activity)context).startActivityForResult(Intent.createChooser(share, "Share..."),1);



    }





//    public static void sharecustom(Context context, String title, String text, Bitmap image){
//
//
//
//
//        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//        // what type of data needs to be send by sharing
//        sharingIntent.setType("text/plain");
//
//        // package names
//        PackageManager pm = context.getPackageManager();
//
//        // list package
//        List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);
//
//
//
//        ShareIntentListAdapter   objShareIntentListAdapter = new  ShareIntentListAdapter(((Activity)context),activityList.toArray());
//
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle("tittle");
//        builder.setAdapter(objShareIntentListAdapter, new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int item) {
//
//
//                final ResolveInfo info = (ResolveInfo) objShareIntentListAdapter.getItem(item);
//
//                // if email shared by user
//                //do the fucking facebook share
//                if (info.activityInfo.packageName.contains("com.facebook.katana")) {
//
//                    ShareLinkContent content = new ShareLinkContent.Builder()
//                            .setContentUrl(Uri.parse("www.alphamatch.com"))
//                            .setContentDescription(text)
//                            .setContentTitle(title)
//                            .setImageUrl(Uri.parse(""))
//                            .build();
//                    ShareDialog shareDialog = new ShareDialog(((Activity)context));
//                    shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
//                    return;
//                    //PullShare.makeRequestEmail(COUPONID,CouponType);
//                } else {
//                    //do normal share
//
//                    Uri imageUri = null;
//                    try {
//                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                        image.compress(Bitmap.CompressFormat.PNG, 100, bytes);
//                        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), image, "Title", null);
//                        imageUri = Uri.parse(path);
//
//                    } catch (Exception ignored) {}
//
//                    ResolveInfo     info1 = (ResolveInfo) objShareIntentListAdapter.getItem(item);
//                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                    intent.setClassName(info1.activityInfo.packageName, info1.activityInfo.name);
//
//                    intent.putExtra(Intent.EXTRA_STREAM, imageUri != null ? imageUri : getLocalBitmapUri(context, image));
//                    intent.putExtra(android.content.Intent.EXTRA_SUBJECT,  title);
//                    intent.putExtra(android.content.Intent.EXTRA_TEXT,text+"  "+"www.google.com");
//                    intent.putExtra(Intent.EXTRA_TITLE, "share");
//                    context.startActivity(intent);
//                }
//            }
//    });
//    }



    private static Uri getLocalBitmapUri(Context context, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            String root = Environment.getExternalStorageDirectory().getAbsolutePath();
            File myDir = new File(root + "/Alpha Match");
            myDir.mkdirs();

            File file =  new File(myDir,  System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            file.setReadable(true, false);
            bmpUri = Uri.fromFile(file);
//            bmpUri = FileProvider.getUriForFile(context, "com.apps.alphamatch", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

}
