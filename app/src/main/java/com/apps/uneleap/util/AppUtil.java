package com.apps.uneleap.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.apps.uneleap.R;


/**
 * Created by  on 01/08/2019.
 */

public class AppUtil {

    public static void startActivityWithAnimation(Activity activity, Intent intent) {
        try {
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } catch (Exception ex) {
        }

    }

    public static void finishActivityWithAnimation(Activity activity) {
        try {
            activity.finish();
            activity.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_out_right);
        } catch (Exception ex) {
        }

    }

    public static void finishActivityWithAnimationRight(Activity activity) {
        try {
            activity.finish();
            activity.overridePendingTransition(R.anim.slide_to_right, R.anim.slide_out_left);
        } catch (Exception ex) {
        }

    }


    public static void startActivityForResultWithAnimation(Activity activity, Intent intent, int
            requestCode) {
        try {
            activity.startActivityForResult(intent, requestCode);
            activity.overridePendingTransition(R.anim.slide_to_right, R.anim.slide_out_left);
        } catch (Exception ex) {
        }

    }

    public static void rotate360(Context context, View view){

        /* Create Animation */
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.image_rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        view.startAnimation(rotation);

    }


}





