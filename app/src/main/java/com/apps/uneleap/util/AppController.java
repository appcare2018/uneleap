package com.apps.uneleap.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AppController {
    public static DateTime_c instance;
    SimpleDateFormat df;

    public AppController() {
        /*if(appValidate("2018-05-12")){
            System.exit(0);
        }*/
    }

    public boolean appValidate(String tilldate) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        final String returnDtm = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        //-----------
        try {
            df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = df.parse(tilldate);
            Date endDate = df.parse(returnDtm);
            long different = endDate.getTime() - startDate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;

            if (elapsedDays > 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
        //----------
    }


}
