package com.apps.uneleap.util;


public class GlobalConstants {

    public static final String USER_ID = "userId";
    public static final String iS_NOTIFICATION = "iS_NOTIFICATION";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_PLAN_TYPE = "user_plan_type";
    public static final String CAN_CREATE_LISTING = "can_create_listing";
    public static boolean event = true;

    public static final String PASSWORD = "userPassword";
    public static final String REMEMBER_CRED = "rememberCred";
    public static final String MODEL = "model";

    public final String Firebasetoken = "Firebasetoken";
    public final String DeviceId = "device_id";
    public final String NOTIFICATION = "Notification";

    public final String LEGALAGREEMENTCHECK = "legalagreement";

    //-------------
    public static final String RESPOSE_CLG_JB_SERVICE = "RESPOSE_CLG_JB_SERVICE";
    public final String ISDOZEDISABLED = "IsDozeDisabled";
    public static final String CURRENT_LATITUDE = "Latest_LATITUDE";
    public static final String CURRENT_LONGITUDE = "Latest_LONGITUDE";
    public static final String CURRENT_Address = "Latest_CurrentAddress";

    public static String LANGUAGE = "language";

    public static String fromPage = "";

    public static final String DEVELOPER_KEY = "AIzaSyDEVY4H8nns2FQSQ07ML6IxbdzmVzIq05c";
    public static final String IN_APP_PURCHASE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoK6bT6JUyOpMqodEetTnyM1ftZ3N03S6v1fTPTEGHTmBqJ+HY27GzXZYj5W12IFJ8DGbtLXwxa8xJGJbeFJ6Mz9r60XQW08GTQ3nfilh56xA9LINiZy7iv/s5kxY9Avvd/V1/ovPQkUJ5ujT9sY3yO+a45aOVVBVgOUHoShZSmFNX0QRvNAimFlgOJwygvRYLEfDG5pF+mcnM/f7zXBw9vkPLi4Eu0DhLpVVA30GcHgfmV4RgN52Z9PMAPU32WWtKRiur+yBGN/2K0II3GfIQPCmIB3/gTjvSMkwskf5Ch8bMzQxqMVhFVrYNtW3FSw87NJ85r+FGg/UQsh+ilTWDQIDAQAB";


    //-------------------
    final public static String gendor_item[] = {"Male/Female","Male", "Female"};
    final public static String sizes_item[] = {"1", "2", "3", "4", "5"};
    final public static String category_item[] = {"All Categories","Pets", "Services", "Products", "Events"};
    final public static String all_sizes_item[] = {"Small", "Medium", "Large", "X-Large", "XX-Large"};
    final public static String all_category_item[] = {"All Categories","Pets", "Services", "Products", "Events"};
    final public static String listing_type_item[] = { "Newest", "Oldest"};

    final public static String payment_type_item[] = { "Export Transactions"};
    final public static String listing_days_item[] = { "30 Days","15 Days","10 Days"};

    //----------------

    final public static String ProfileIMGURL = "profileImageUrl";
    final public static String AccessToken = "AccessToken";
    final public static String IS_LOGIN = "isLogin";
    final public static String IS_FB_LOGIN = "isFBLogin";
    final public static String isEmailNotification = "isEmailNotification";
    final public static String isPushNotification = "isPushNotification";

    //-----------------
    final public static String UPGRADE_BACK_BTN = "upgradeBackbtn";

    //----------------addListing Bundle keys constants
    final public static String EDIT_LISTING_MODEL = "editListingModel";
    final public static String EDIT_LISTING_ID = "editListingId";
    final public static String CATEGORY = "category";
    final public static String SUB_CATEGORY = "subCategory";
    final public static String CATEGORY_VALUE = "categoryValue";
    final public static String SUB_CATEGORY_VALUE = "subCategoryValue";
    final public static String CRITERIA = "criteria";
    final public static String SUB_CRITERIA = "subCriteria";
    final public static String CRITERIA_VALUE = "criteriaValue";
    final public static String SUB_CRITERIA_VALUE = "subCriteriaValue";
    final public static String TITLE = "title";
    final public static String DESCRIPTION = "description";
    final public static String LISTINGTYPE = "listingType";
    final public static String GENDER = "gender";
    final public static String BREED = "breed";
    final public static String AGE = "age";
    public static final String SIZE = "size";
    final public static String REGISTRATION = "registration";
    final public static String FEATURE_PHOTO = "featurePhoto";
    final public static String YOUTUBE_URL = "youtubeUrl";
    final public static String PRICE = "price";
    final public static String PREFIX = "prefix";
    final public static String SUFIX = "sufix";
    final public static String CUSTOM_PRICING_MESSAGE = "customPricingMessage";
    final public static String STATE = "province";
    final public static String CITY = "city";
    final public static String ZIPCODE = "zipcode";
    final public static String WEBSITE_LINK = "websiteLink";
    final public static String SOCIAL_LINK = "sociallink";
    final public static String INSAGRAM = "instagram";
    final public static String TWITTER = "twitter";
    final public static String FACEBOOK = "facebook";
    final public static String PAYMENT_METHOD = "payentMethod";
    final public static String TERMS_OF_SERVICE = "termsOfService";
    final public static String MORE_VIEWS_TYPE = "moreViewstype";
    final public static String EVENT_DATE = "eventDate";
    final public static String EVENT_TIME = "eventTime";
    final public static String AGE_YEAR = "ageYear";
    final public static String AGE_MONTH = "ageMonth";
    final public static String AGE_WEEK = "ageWeek";

    public static final int LOCATION_REQUEST = 1000;
    public static final int GPS_REQUEST = 1001;


}
