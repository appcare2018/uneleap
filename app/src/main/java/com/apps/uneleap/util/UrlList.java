package com.apps.uneleap.util;

public class UrlList {


    //************************************************************************************************
    final public static String domainURL = "http://ec2-13-234-249-108.ap-south-1.compute.amazonaws.com:8188/alpha/api/";
    final public static String version = "v1/";


    final public static String registration_step = domainURL+version + "user/createUser";
    final public static String login = domainURL+ version + "user/login";
    final public static String getUserProfile = domainURL+ version + "user/getUserProfile";
    final public static String getUpdateProfile = domainURL+ version + "user/updateUser";


}
