package com.apps.uneleap.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;


import com.apps.uneleap.R;
import com.apps.uneleap.activity.MainActivity;
import com.apps.uneleap.controller.InpuClass;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static com.apps.uneleap.util.GlobalConstants.event;


public class CommonFunctions extends InpuClass {
    private static ProgressDialog progDialog;
    public final GlobalConstants gc = new GlobalConstants();
    public static MySharedPereference myPreference = MySharedPereference.getInstance();
    public static String VisibleFragmentNm = "";
    public static Context context;
    public ConnectionDetector cdr;
    public static Bundle bdl = new Bundle();
    public UrlList urlList;
    public AddressFromLatLong addressfrmltlg;
    AppController appController = new AppController();


    public CommonFunctions(Context mcontext) {
        this.context = mcontext;


        addressfrmltlg = new AddressFromLatLong(context);
        cdr = new ConnectionDetector(context);
    }

    public Context getContext() {
        return this.context;
    }

    public static boolean replaceFragment(Context newContext, Fragment fragment, String TagName) {
        try {

//            String backStateName = fragment.getClass().getName();
//
//            FragmentManager manager = ((MainActivity) newContext).getSupportFragmentManager();
//            boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);
//
//            if (!fragmentPopped){ //fragment not in back stack, create it.
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.fragment_container, fragment,TagName);
//                ft.addToBackStack(backStateName);
//                ft.commit();
//            }

            VisibleFragmentNm = TagName;
            FragmentManager manager = ((MainActivity) newContext).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Fragment currentFrag = manager.findFragmentByTag(TagName);

            if (currentFrag != null && currentFrag.getClass().equals(fragment.getClass())) {
                transaction.replace(R.id.fragment_container, fragment, TagName).commit();
            } else {
                transaction.replace(R.id.fragment_container, fragment, TagName).addToBackStack(null).commit();
            }




//            transaction.replace(R.id.fragment_container, fragment, TagName); //remove
//            // manager.popBackStack();
//            transaction.addToBackStack(null);
//            transaction.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void clearAllFragment(Context newContext){
        // need to be test to clear all fragment
       /* FragmentManager fm = ((MainActivity) newContext).getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }*/
    }

    public void dropDownHeight(Spinner spinner, int height){
        try {

            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            //popupWindow.setHeight(height);



        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
    }


    public static void back_fragment() {
        try {
          final   FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
            int count = manager.getBackStackEntryCount();
            if (count != 1) {
                manager.popBackStackImmediate();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void back_fragment(Context context) {
        try {
          final   FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
            int count = manager.getBackStackEntryCount();
            if (count != 1) {
                manager.popBackStackImmediate();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void removeUpgradeAccountFragment(){
        FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
        for(int i = 0; i < 3; ++i) {
            manager.popBackStack();
        }
//        for(int i = 0; i < manager.getBackStackEntryCount(); ++i) {
//            fm.popBackStack();
//        }


//        Fragment upgradeAccount = manager.findFragmentByTag("UpgradeAccount_Fr");
//        if(upgradeAccount!= null) {
//            manager.beginTransaction().remove(upgradeAccount).commit();
//        }
//
//        Fragment upgradeAccountPrivacy = manager.findFragmentByTag("Privacy_fr");
//        if(upgradeAccountPrivacy!= null){
//            manager.beginTransaction().remove(upgradeAccountPrivacy).commit();
//        }
//
//        Fragment upgradeAccountDetail = manager.findFragmentByTag("UpgradeAcDetail_fr");
//        if(upgradeAccountDetail!= null){
//            manager.beginTransaction().remove(upgradeAccountDetail).commit();
//        }
//
//        Fragment upgradeAccountInitPayment = manager.findFragmentByTag("InitPayment_fr");
//        if(upgradeAccountInitPayment!= null){
//            manager.beginTransaction().remove(upgradeAccountInitPayment).commit();
////            FragmentTransaction transaction = manager.beginTransaction();
////            transaction.remove(upgradeAccountInitPayment);
////            transaction.commit();
//            manager.popBackStack("InitPayment_fr", FragmentManager.POP_BACK_STACK_INCLUSIVE);
////            manager.popBackStackImmediate("InitPayment_fr", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }

    }

    public String getLastnCharacters(String inputString,
                                     int subStringLength) {
        int length = inputString.length();
        if (length <= subStringLength) {
            return inputString;
        }
        int startIndex = length - subStringLength;
        return inputString.substring(startIndex);
    }

    private String phoneValidate(final String phon) {
        String phn = phon.replaceAll("\\s+", "");
        String pn = "";
        try {
            if (phn.contains("+91")) {
                pn = phn.replace("+91", "");
            } else if (phn.contains("+1")) {
                pn = phn.replace("+1", "");
            } else if (phn.contains("(")) {
                pn = phn.replaceAll("[()]", "");
                pn = pn.replaceAll("-", "");
            }
        } catch (Exception ex) {
            pn = phn;
        }

        return pn;
    }

    public boolean isEmpty(String string) {
        try {
            if (string == null || string.trim().length() == 0) {
                return true;

            } else {
                return false;
            }
        } catch (Exception Ex) {
            return true;
        }
    }

    public void hideKeyboard(View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception Ex) {
        }
    }

    public void openKeyboard(View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        } catch (Exception Ex) {
        }
    }


    public void startAc(Context context, Class cls, final Bundle bundle) {
        if (event) {
            event = false;
            final Intent intent = new Intent(context, cls);
            if (bundle != null)
                intent.putExtras(bundle);
            context.startActivity(intent);
            //  ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        new Handler().postDelayed(() -> event = true, 200);
    }


    public String device_id() {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // Toast.makeText(this, deviceId, Toast.LENGTH_SHORT).show();
        return deviceId;
    }

    public Map<String, String> push_registration(String user_id, String token_id) {
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        params.put("device_id", device_id());
        params.put("token_id", token_id);
        return params;
    }


    public Map<String, String> api_Registration(String firstName,String lastName, String email,String password,String firebasetoken) {
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("email", email);
        params.put("password", password);
        params.put("isConnectedWithFb", "false");
        params.put("deviceToken", device_id());
        params.put("fcmId", firebasetoken);

        return params;
    }
    public Map<String, String> api_Login( String email,String password,String firebaseToken) {
        final HashMap<String, String> params = new HashMap<String, String>();

        params.put("email", email);
        params.put("password", password);
        params.put("loginType", "true");
        params.put("deviceToken", firebaseToken);

        return params;
    }

    public Map<String, String> get_Profile( String accessToken) {
        final HashMap<String, String> params = new HashMap<String, String>();

        params.put("Content-Type", "application/json");
        params.put("accessToken", accessToken);

        return params;
    }

    public Map<String, String> get_Profile_HeaderParam( String accessToken) {
        final HashMap<String, String> params = new HashMap<String, String>();

        // params.put("Content-Type", "application/json");
        params.put("accessToken", accessToken);

        return params;
    }

    public Map<String, String> update_Profile_Step( String oldEmail,String email,String oldMobile,String mobile,String website,String address,String city,String zip,String country) {
        final HashMap<String, String> params = new HashMap<String, String>();

        // params.put("Content-Type", "application/json");
        params.put("oldEmail", oldEmail);
        params.put("email", email);
        params.put("oldMobile", oldMobile);
        params.put("mobile", mobile);
        params.put("website", website);
        params.put("address", address);
        params.put("city", city);
        params.put("zipcode", zip);
        params.put("country", country);

        return params;
    }

    public Map<String, String> update_Profile_Step3( String facebookId,String instagramId,String snapchatId,String youtubeUrl) {
        final HashMap<String, String> params = new HashMap<String, String>();

        // params.put("Content-Type", "application/json");
        params.put("facebookId", facebookId);
        params.put("instagramId", instagramId);
        params.put("snapchatId", snapchatId);
        params.put("youtubeUrl", youtubeUrl);


        return params;
    }
    public Map<String, String> get_Profile1() {
        final HashMap<String, String> params = new HashMap<String, String>();


        return params;
    }

    public Map<String, String> get_ProfileMain() {
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("firstName", "dev");
        params.put("lastName", "pra");

        return params;
    }


    public Map<String, String> user_id(String user_id) {
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        return params;
    }


    public final String doubleToString(final double dbl_vlue) {
        String string_value = "";
        try {
            string_value = String.valueOf(dbl_vlue);
        } catch (Exception Ex) {
        }
        return string_value;
    }


    public Map<String, String> empty_() {
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("om's", "om's");
        return params;
    }


    //======================
    public Map<String, String> getCurrentLocation() {
        final HashMap<String, String> params = new HashMap<String, String>();
        final String user_id = myPreference.getString(context, gc.USER_ID);
        String Latitude = myPreference.getString(context, gc.CURRENT_LATITUDE);
        String Longitude = myPreference.getString(context, gc.CURRENT_LONGITUDE);
        String PointAddress = myPreference.getString(context, gc.CURRENT_Address);
        final String date = new DateTime_c().currentDateTime("yyyy/MM/dd");
        final String time = new DateTime_c().currentDateTime("HH:mm:ss");
        if (isEmpty(PointAddress)) {
            PointAddress = addressfrmltlg.getAddress();
            if (isEmpty(PointAddress)) {
                Latitude = "";
                Longitude = "";
                PointAddress = "";
            }
        }
        params.put("lat", Latitude);
        params.put("lng", Longitude);
        params.put("address", PointAddress);
        params.put("date", date);
        params.put("time", time);
        params.put("user_id", user_id);

        return params;
    }

    public void setBundleData(String key,String val){
        bdl.putString(key, val);
    }
    public String getBundleData(String key){
        String storedVal ="";
        if (bdl != null) {
            storedVal = bdl.getString(key);

        }
        return storedVal;
    }
    public void setBundleDataInteger(String key,int val){
        bdl.putInt(key, val);
    }
    public int getBundleDataInteger(String key){
        int storedVal =0;
        if (bdl != null) {
            storedVal = bdl.getInt(key);

        }
        return storedVal;
    }
    public void setBundleDataBoolean(String key,Boolean val){
        bdl.putBoolean(key, val);
    }

    public boolean getBundleDataBoolean(String key){
        boolean storedVal =true;
        if (bdl != null) {
            storedVal = bdl.getBoolean(key);

        }
        return storedVal;
    }


    public static String formattedDate(String inputDate){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }

    public static String localFormattedDate(String inputDate){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }

    public static String localFormattedWithoutDate(String inputDate){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM. yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setChatStatusBarBlack(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        }
    }

    public static void showProgress(Context context){
        if (!((Activity)context).isFinishing()) {
            progDialog = ProgressDialog.show(context, null, null, false, true);
            progDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progDialog.setContentView(R.layout.progress_dialog);
            progDialog.show();
        }
    }

    public static void hideProgress(Context context){
        if (!((Activity)context).isFinishing()) {
            progDialog.dismiss();
        }
    }


    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed/)[^#&?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if(matcher.find()){
            return matcher.group();
        } else {
            return "error";
        }
    }



    public static String dateConvert(long milliSeconds)
    {
       // String dateFormat= "dd/MM/yyyy";
        String dateFormat= "MMM yyyy";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}