package com.apps.uneleap.apiPackage;


import com.apps.uneleap.controller.pojo.NotificationModel;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.ForgetPasswordResponseModel;
import com.apps.uneleap.model.GetAnswerByQuestionResponseModel;
import com.apps.uneleap.model.GetCommentByAnswerResponseModel;
import com.apps.uneleap.model.LoginResponseModel;
import com.apps.uneleap.model.profile.ProfileEducationModel;
import com.apps.uneleap.model.profile.ProfileExperienceModel;
import com.apps.uneleap.model.profile.ProfileModel;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.model.RegistrationModel;
import com.apps.uneleap.model.TopicsResponseModel;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Debi on 02-10-2019.
 */
public interface ApiInterface {

    //*********************************** Login ***********************************
    @POST("api/login")
    Call<LoginResponseModel> login(@Body RequestBody tdt);

    //*********************************** Login Facebook ***********************************

    @POST("api/registration")
    Call<RegistrationModel> registration(@Body RequestBody tdt);


    @POST("api/forgetpassword")
    Call<ForgetPasswordResponseModel> forgetPassword(@Body RequestBody tdt);

    //*********************************** AllTopics ***********************************
    @POST("api/showAllTopics")
    Call<TopicsResponseModel> getAllTopics();

    //*********************************** AllQuestion ***********************************
    @POST("apis/question/getQuestions")
    Call<QuestionResponseModel> getQuestion(@Body RequestBody tdt);

    //*********************************** getAnswersByQuestionId ***********************************
    @POST("apis/question/getAnswersByQuestionId")
    Call<GetAnswerByQuestionResponseModel> getAnswersByQuestionId(@Body RequestBody tdt);

    //*********************************** getCommentsByAnswerId ***********************************
    @POST("apis/question/getCommentsByAnswerId")
    Call<GetCommentByAnswerResponseModel> getCommentsByAnswerId(@Body RequestBody tdt);


    //*********************************** add Question ***********************************
    @POST("apis/question/postQuestion")
    Call<BaseResponseModel> addQuestion(@Body RequestBody tdt);

    //*********************************** add Answer ***********************************
    @POST("apis/question/postAnswer")
    Call<BaseResponseModel> addAnswer(@Body RequestBody tdt);

    //*********************************** add likeDislikeAnswer ***********************************
    @POST("apis/question/likeDislikeAnswer")
    Call<BaseResponseModel> likeDislikeAnswer(@Body RequestBody tdt);

    //*********************************** add likeDislikeComment ***********************************
    @POST("apis/question/likeDislikeComment")
    Call<BaseResponseModel> likeDislikeComment(@Body RequestBody tdt);


    @POST("apis/question/likeDislikeAnswer")
    Call<JsonObject> likeDislikeAnswerJson(@Body RequestBody tdt);

    //*********************************** post postCommentOfAnswer ***********************************
    @POST("apis/question/postCommentOfAnswer")
    Call<BaseResponseModel> postCommentOfAnswer(@Body RequestBody tdt);

    @POST("apis/question/likeDislikeAnswer")
    Call<NotificationModel> newsWorldWide(@Body HashMap<String, Object> body);

    @POST("api/getUserDetail")
    Call<ProfileModel> getProfile(@Body HashMap<String, Object> body);

    @POST("apis/education/getUserEducation")
    Call<ProfileEducationModel> getUserEducation(@Body HashMap<String, Object> body);

    @POST("apis/experience/getUserExperience")
    Call<ProfileExperienceModel> getUserExperience(@Body HashMap<String, Object> body);

    @POST("apis/experience/postExperience")
    Call<ProfileExperienceModel> postExperience(@Body HashMap<String, Object> body);




}
