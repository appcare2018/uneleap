package com.apps.uneleap.apiPackage;

/**
 * Created by Gaurav on 16-05-2019.
 */
public interface ApiRequestConstant {

    ApiInterface API_INTERFACE = ApiClient.getClient().create(ApiInterface.class);
    ApiInterface API_INTERFACE_THIRD_PARTY = ApiClient.getClientForThirdParty().create(ApiInterface.class);

    int LOGIN_USER = 100;
    int TOPICS = 101;
    int QUESTIONS = 102;
    int ADDQUESTIONS = 103;
    int LIKEDISLIKE = 104;
    int POSTCOMMENT = 105;

}
