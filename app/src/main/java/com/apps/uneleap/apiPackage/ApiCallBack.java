package com.apps.uneleap.apiPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.WindowManager;

import com.apps.uneleap.util.Utils;

import java.net.SocketTimeoutException;
import java.util.Objects;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gaurav on 16-05-2019.
 */

public class ApiCallBack<T> implements Callback<T> {

    private Context context;
    private ApiResponseErrorCallback responseErrorCallback;
    private int flag;
    ProgressDialog progressDialog ;
    boolean isCustomProgress=false;
    public ApiCallBack(Context context, ApiResponseErrorCallback responseErrorCallback, int flag, boolean isCustomProgress) {
        this.context = context;
        this.responseErrorCallback = responseErrorCallback;
        this.flag = flag;
        this.isCustomProgress=isCustomProgress;
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading...");
        if(isCustomProgress){
            /*show progress dialog if want*/
            progressDialog.show();
        }
    }
    public ApiCallBack(Context context, ApiResponseErrorCallback responseErrorCallback, int flag) {
        this.context = context;
        this.responseErrorCallback = responseErrorCallback;
        this.flag = flag;

    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (isCustomProgress)
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            responseErrorCallback.getApiResponse(response.body(), flag);
        } else
            Utils.showToast(context, "Server Not Responding");

        if (response.body() != null)
            Log.e("@@Respnse@@", Objects.requireNonNull(response.body()).toString());

    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (isCustomProgress)
        progressDialog.dismiss();
        Log.e("@@Error@@", t.toString());
        responseErrorCallback.getApiError(t, flag);

        if (t instanceof SocketTimeoutException) {
            if (context instanceof Activity)
                ((Activity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

}