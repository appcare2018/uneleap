package com.apps.uneleap.apiPackage;

/**
 * Created by Debi on 02-10-2019.
 */
public interface ApiResponseErrorCallback {
    void getApiResponse(Object object, int flag);
    void getApiError(Throwable t, int flag);
}
