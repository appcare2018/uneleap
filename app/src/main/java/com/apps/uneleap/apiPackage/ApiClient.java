package com.apps.uneleap.apiPackage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gaurav on 16-05-2019.
 */

public class ApiClient {

    //*********************Internal url ********************************
    //FOR DEVELOPMENT
    public static final String BASE_URL = "http://198.57.156.77/~spa2go/unleap/";



    //*********************Production url ********************************
//    public static final String BASE_URL = "";


    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(0,loggingInterceptor);

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static RequestBody getRequestValue(JSONObject requestObject){
        RequestBody myReqBody = null;
               try {
                      return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                       (new JSONObject(String.valueOf(requestObject))).toString());
                    } catch (JSONException e) {
                      e.printStackTrace();
                    }
                return myReqBody;
           }

    public static Retrofit getClientForThirdParty() {

        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(0,loggingInterceptor);

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
