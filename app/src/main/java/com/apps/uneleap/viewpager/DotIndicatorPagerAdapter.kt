package com.apps.uneleap.viewpager


import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.apps.uneleap.R
import com.apps.uneleap.databinding.MaterialPageBinding


class DotIndicatorPagerAdapter : PagerAdapter() {

  var pos=0

  lateinit var materialPageBinding: MaterialPageBinding
  lateinit var walkthroughResourcesArray: Array<Int>
  lateinit var walkthroughTitlesArray: Array<String>
  fun newInstance(res: Resources): DotIndicatorPagerAdapter {

    walkthroughResourcesArray = arrayOf(R.drawable.walkthrough_01_forum, R.drawable.walkthrough_02_news, R.drawable.walkthrough_03_community, R.drawable.walkthrough_04_events, R.drawable.walkthrough_05_library)
    walkthroughTitlesArray = res.getStringArray(R.array.walkthrough_titles)
    return this
  }



  override fun instantiateItem(container: ViewGroup, position: Int): Any {

    val item = LayoutInflater.from(container.context).inflate(R.layout.material_page, container, false)
    materialPageBinding = DataBindingUtil.bind(item)!!

    materialPageBinding.walkthroughTitleTV.setText(walkthroughTitlesArray[position])
    materialPageBinding.walkthroughIV.setImageResource(walkthroughResourcesArray[position])
    ViewPagerActivity.pagerController.callback(position)
    container.addView(item)

   // Utils.showToast(container.context,"init"+position )
    pos=position

    return item

  }

  override fun getCount(): Int {
    return walkthroughTitlesArray.size
  }

  override fun isViewFromObject(view: View, `object`: Any): Boolean {

    return view === `object`
  }

  override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
    //Utils.showToast(container.context,""+position+pos )
    ViewPagerActivity.pagerController.callback(position+pos)
    container.removeView(`object` as View)

  }
}
