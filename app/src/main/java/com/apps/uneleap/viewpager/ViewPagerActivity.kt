package com.apps.uneleap.viewpager

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.apps.uneleap.R
import com.apps.uneleap.activity.GetStarted
import com.apps.uneleap.util.Controller
import kotlinx.android.synthetic.main.activity_view_pager.*


//import kotlinx.android.synthetic.main.activity_view_pager.spring_dots_indicator

//import kotlinx.android.synthetic.main.activity_view_pager.worm_dots_indicator

class ViewPagerActivity : AppCompatActivity() {

   // val menuTitleTextView by lazy { findViewById<TextView>(R.id.tv_next) }
    companion object {
       @JvmStatic lateinit  var pagerController: Controller
        //val info = menu
        fun getMoreInfo():String { return "This is more fun" }
    }
    init {
        pagerController = Controller { resultz ->

            if(resultz==6){
                tv_next.text="Done"
            }

            else{
                tv_next.text="Skip"
        }
            
        }
    }
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
      window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
          val attrib = window.attributes
          attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
      } else
          window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
    setContentView(R.layout.activity_view_pager)



      with(view_pager) {
          adapter = DotIndicatorPagerAdapter().newInstance(this.resources)
      setPageTransformer(true, ZoomOutPageTransformer())

      dots_indicator.setViewPager(this)
      //spring_dots_indicator.setViewPager(this)
      //worm_dots_indicator.setViewPager(this)

    }


  }


  fun skip(view: View) {
    val intent = Intent(this, GetStarted::class.java)
    startActivity(intent)
  }
}
