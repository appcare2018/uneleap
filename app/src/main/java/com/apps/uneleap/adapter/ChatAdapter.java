package com.apps.uneleap.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.databinding.ChatAdapterBinding;
import com.apps.uneleap.util.CommonFunctions;
import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private ArrayList<String> listingDetails;
    private Context context;
    private CommonFunctions cmf;
    ChatAdapterBinding chatAdapterBinding;

    public ChatAdapter(Context context) {
        this.context = context;
        cmf = new CommonFunctions(context);
        //this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.chat_adapter, parent, false);
        chatAdapterBinding = DataBindingUtil.bind(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
//        holder.bindData(context, position);
        if (position == 0 || position == 1 || position == 2)
            chatAdapterBinding.setNewChat(true);
        else
            chatAdapterBinding.setNewChat(false);

        chatAdapterBinding.executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return 10;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View v) {
            super(v);

        }

        void bindData(Context context, int position) {

        }
    }

    @BindingAdapter("isBold")
    public static void setBold(TextView view, boolean isBold) {
        if (isBold) {
            view.setTypeface(null, Typeface.BOLD);
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }
    }

    @BindingAdapter("isVisible")
    public static void setVisibility(TextView view, boolean isVisible) {

        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);

    }
}