package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.model.profile.ProfileEducationModel;
import com.apps.uneleap.model.profile.ProfileExperienceModel;
import com.apps.uneleap.util.CommonFunctions;

import java.util.List;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class ProfileExpAdapter extends RecyclerView.Adapter<ProfileExpAdapter.ViewHolder> {
    private Context context;
    private int LAYOUT;
    List<ProfileExperienceModel.Datum> listData;
    List<ProfileEducationModel.Datum> listEud;
    int datasize;

    private CommonFunctions cmf;

    public ProfileExpAdapter(Context context, List<ProfileExperienceModel.Datum> listData, int LAYOUT, CommonFunctions cmf) {
        this.context = context;
        this.listData = listData;
        this.cmf = cmf;
        this.LAYOUT = LAYOUT;
        datasize=listData.size();
    }

    public ProfileExpAdapter(List<ProfileEducationModel.Datum> data, int LAYOUT, CommonFunctions cmf) {
        this.context = cmf.getContext();
        this.listEud = data;
        this.cmf = cmf;
        this.LAYOUT = LAYOUT;
        datasize=listEud.size();
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(LAYOUT, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if(listData!=null) {
            holder.tvView.setText(listData.get(position).getTitle());
            holder.tvView1.setText(listData.get(position).getUniversity());
            holder.tvView2.setText(cmf.dateConvert(Long.parseLong(listData.get(position).getStartDate()))+" - "+
                    cmf.dateConvert(Long.parseLong(listData.get(position).getEndDate())));
            holder.tvView3.setText(listData.get(position).getLocation());
        }else {
            holder.tvView.setText(listEud.get(position).getSchoolName());
            holder.tvView1.setText(listEud.get(position).getDegree());
            holder.tvView2.setText(cmf.dateConvert(Long.parseLong(listEud.get(position).getStartDate()))+" - "+
                    cmf.dateConvert(Long.parseLong(listEud.get(position).getEndDate())));


        }
    }


    @Override
    public int getItemCount() {
        return datasize;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivContentImage;
        TextView tvView,tvView1,tvView2,tvView3;
        CardView ivStar;

        public ViewHolder(View v) {
            super(v);
            tvView = v.findViewById(R.id.tvName);
            tvView1 = v.findViewById(R.id.tvUniversity);
            tvView2 = v.findViewById(R.id.tvDate);
            tvView3 = v.findViewById(R.id.tvLocation);
        }
    }


}