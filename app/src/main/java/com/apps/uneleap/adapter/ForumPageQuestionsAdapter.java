package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.databinding.QuestionItemBinding;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import static com.apps.uneleap.util.Utils.convertDate;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class ForumPageQuestionsAdapter extends RecyclerView.Adapter<ForumPageQuestionsAdapter.ViewHolder> {

    private ArrayList<QuestionResponseModel.Data> listingDetails;
    private Context context;
    private CommonFunctions cmf;

    public interface OnItemClickListener {
        void onItemClick(int isClick, String isLike, QuestionResponseModel.Data listingDetails);
    }

    private final OnItemClickListener listener;

    public ForumPageQuestionsAdapter(Context context, ArrayList<QuestionResponseModel.Data> listingDetails, OnItemClickListener listener) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listener = listener;
        this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_page_topics_items, parent, false);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        QuestionItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.question_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bindData(position);


    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public QuestionItemBinding binding;


        public ViewHolder(QuestionItemBinding v) {
            super(v.getRoot());
            binding = v;

        }

        void bindData(int position) {
            //binding.tvTitle.setText(listingDetails.get(position).getTitle());
            binding.tvQuestion.setText(listingDetails.get(position).getQuestion());
            if (listingDetails.get(position).getMost_like_answer().getAnswered_by() != null) {
                binding.tvName.setText("" + listingDetails.get(position).getMost_like_answer().getAnswered_by().getFirst_name() + " " +
                        listingDetails.get(position).getMost_like_answer().getAnswered_by().getLast_name());

            }

            if (listingDetails.get(position).getMost_like_answer().getAnswer() != null) {
                try {
                    Glide.with(context)
                            .load(listingDetails.get(position).getMost_like_answer().getAnswered_by().getImage_url())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .placeholder(R.drawable.profile_image)
                            .error(R.drawable.profile_image)
                            .into(binding.profilePic);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                binding.tvAnswer.setText(listingDetails.get(position).getMost_like_answer().getAnswer());
                binding.tvLike.setText(listingDetails.get(position).getMost_like_answer().getLikes());
                binding.tvComment.setText(listingDetails.get(position).getMost_like_answer().getComments());
                binding.tvAnsweredDate.setText("Answered " + convertDate(listingDetails.get(position).getMost_like_answer().getEdate(), "MMM dd yyyy"));

                if (listingDetails.get(position).getMost_like_answer().isLiked_by_you())
                    binding.ivLike.setChecked(true);

                binding.ivLike.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (listingDetails.get(position).getMost_like_answer().getAnswer_id() != null) {
                        if (isChecked)
                            listener.onItemClick(3, "1", listingDetails.get(position));
                        else
                            listener.onItemClick(3, "0", listingDetails.get(position));
                    }
                });

            }

//            binding.tvLike.setOnClickListener(v->{
//
//                likeAPICall();
//                Intent intent=new Intent(context,ForumComment.class);
//
//                intent.putExtra("QuestionResponseModel", listingDetails.get(position));
//                context.startActivity(intent);
//
//            });
            binding.tvQuestion.setOnClickListener(view ->
                    listener.onItemClick(0, "", listingDetails.get(position)));

            binding.tvComment.setOnClickListener(view ->
                    listener.onItemClick(2, "", listingDetails.get(position)));
        }
    }

    private void likeAPICall() {
    }


}