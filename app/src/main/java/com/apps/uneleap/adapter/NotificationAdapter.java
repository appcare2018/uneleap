package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.databinding.NotificationChatItemBinding;
import com.apps.uneleap.databinding.NotificationChatRecyclerBinding;
import com.apps.uneleap.databinding.NotificationForumItemBinding;
import com.apps.uneleap.databinding.NotificationItemsHeadersBinding;
import com.apps.uneleap.databinding.NotificationLatestNewsItemBinding;
import com.apps.uneleap.model.NotificationChatModel;
import com.apps.uneleap.model.NotificationForumModel;
import com.apps.uneleap.model.NotificationNewsModel;
import com.apps.uneleap.model.NotificationTitleModel;
import com.apps.uneleap.util.CommonFunctions;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<Object> notifications;
    private Context context;
    private CommonFunctions cmf;


    private NotificationItemsHeadersBinding notificationItemsHeadersBinding;
    private NotificationForumItemBinding notificationForumItemBinding;
    private NotificationLatestNewsItemBinding notificationLatestNewsItemBinding;
    private NotificationChatRecyclerBinding notificationChatRecyclerBinding;

    public NotificationAdapter(Context context, ArrayList<Object> notifications) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.notifications = notifications;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.notification_items_headers, parent, false);
                notificationItemsHeadersBinding = DataBindingUtil.bind(view);
                break;
            case 1:
                view = layoutInflater.inflate(R.layout.notification_latest_news_item, parent, false);
                notificationLatestNewsItemBinding = DataBindingUtil.bind(view);
                break;
            case 2:
                view = layoutInflater.inflate(R.layout.notification_forum_item, parent, false);
                notificationForumItemBinding = DataBindingUtil.bind(view);
                break;
            case 3:
                view = layoutInflater.inflate(R.layout.notification_chat_recycler, parent, false);
                notificationChatRecyclerBinding = DataBindingUtil.bind(view);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Object item = notifications.get(position);
        switch (getItemViewType(position)){
            case 0:
                notificationItemsHeadersBinding.setNotificationTitleModel((NotificationTitleModel) item);
                notificationItemsHeadersBinding.executePendingBindings();
                break;
            case 1:
                notificationLatestNewsItemBinding.setNotificationLatestNews((NotificationNewsModel) item);
                notificationLatestNewsItemBinding.executePendingBindings();
                break;
            case 2:
                notificationForumItemBinding.setNotificationForumModel((NotificationForumModel) item);
                notificationForumItemBinding.executePendingBindings();
                break;
            case 3:
                NotificationChatAdapter notificationChatAdapter = new NotificationChatAdapter(context,(ArrayList<NotificationChatModel>)item);
                notificationChatRecyclerBinding.chatRV.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
                notificationChatRecyclerBinding.chatRV.setAdapter(notificationChatAdapter);
                notificationChatRecyclerBinding.executePendingBindings();
                break;
        }

    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View v) {
            super(v);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (notifications.get(position) instanceof NotificationTitleModel) {
            return 0;
        } else if (notifications.get(position) instanceof NotificationLatestNewsItemBinding) {
            return 1;
        } else if (notifications.get(position) instanceof NotificationForumModel) {
            return 2;
        } else if (notifications.get(position) instanceof ArrayList) {
            return 3;
        }
        return 1;
    }
}