package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.uneleap.R;
import com.apps.uneleap.util.CommonFunctions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class EventPageEventAdapter extends RecyclerView.Adapter<EventPageEventAdapter.ViewHolder>  {

    private ArrayList<String> listingDetails;
    private Context context;
    private CommonFunctions cmf;

    public EventPageEventAdapter(Context context, List<String> listingDetails) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_page_events_items, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bindData(context, position);


    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }



    public  class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View v) {
            super(v);

        }

        void bindData(Context context, int position) {

        }
    }


}