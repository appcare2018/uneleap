package com.apps.uneleap.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.uneleap.R;
import com.apps.uneleap.activity.forum.ForumComment;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ForumCommentItemBinding;
import com.apps.uneleap.databinding.QuestionItemBinding;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.GetCommentByAnswerResponseModel;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.LIKEDISLIKE;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.apps.uneleap.util.Utils.convertDate;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>  {

    private ArrayList<GetCommentByAnswerResponseModel.Data> listingDetails;
    private Context context;
    private CommonFunctions cmf;
    public interface OnItemClickListener {
        void onItemClick(boolean isQuestionClick, int id, String question);
    }
    private final OnItemClickListener listener;

    public CommentAdapter(Context context, ArrayList<GetCommentByAnswerResponseModel.Data> listingDetails, OnItemClickListener listener) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listener = listener;
        this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_page_topics_items, parent, false);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ForumCommentItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.forum_comment_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bindData(position);


    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }



    public  class ViewHolder extends RecyclerView.ViewHolder {
      public   ForumCommentItemBinding binding;


        public ViewHolder(ForumCommentItemBinding v) {
            super(v.getRoot());
            binding =v;

        }

        void bindData(int position) {
            //binding.tvTitle.setText(listingDetails.get(position).getTitle());
            try {
                Glide.with(context)
                        .load(listingDetails.get(position).getComment_by().getImage_url())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .placeholder(R.drawable.profile_image)
                        .error(R.drawable.profile_image)
                        .into(binding.ivpc);



            }
            catch (Exception ex){
                ex.printStackTrace();
            }

            String sourceString = "<b>" + listingDetails.get(position).getComment_by().getFirst_name() +" "+listingDetails.get(position).getComment_by().getFirst_name() + "</b> " +" "+listingDetails.get(position).getComment();
            binding.tvName.setText(Html.fromHtml(sourceString));

            if (listingDetails.get(position).isLiked_by_you())
                binding.ivLike.setChecked(true);

            binding.ivLike.setOnCheckedChangeListener((buttonView, isChecked) -> {

                if (listingDetails.get(position).getAnswer_comment_id()!=null){
                    if (isChecked)
                        likeDislike(listingDetails.get(position).getAnswer_comment_id(),"1");
                    else
                        likeDislike(listingDetails.get(position).getAnswer_comment_id(),"0");
                }


            });



        //  listener.onItemClick(false,Integer.parseInt(listingDetails.get(position).getQuestion_id()),listingDetails.get(position).getQuestion()));
        }
    }

    private void likeDislike(String answerId,String isLike){

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis();

            params.put("user_id", CommonFunctions.myPreference.getString(context, USER_ID));
            params.put("answer_comment_id", answerId);
            params.put("like", isLike);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.likeDislikeComment(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(context, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            BaseResponseModel responseModel = (BaseResponseModel) object;
                            if(responseModel.getStatus()==200){
                                Utils.showToast(context, responseModel.getMessage());
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, LIKEDISLIKE, true)
            );
        }else {
            Utils.showToast(context, context.getResources().getString(R.string.no_internet_error));
        }
    }



}