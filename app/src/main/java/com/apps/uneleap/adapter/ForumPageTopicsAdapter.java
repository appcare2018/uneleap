package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.uneleap.R;
import com.apps.uneleap.model.TopicsResponseModel;
import com.apps.uneleap.util.CommonFunctions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class ForumPageTopicsAdapter extends RecyclerView.Adapter<ForumPageTopicsAdapter.ViewHolder>  {

    private ArrayList<TopicsResponseModel.Data> listingDetails;
    private Context context;
    private CommonFunctions cmf;

    public ForumPageTopicsAdapter(Context context, ArrayList<TopicsResponseModel.Data> listingDetails) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_page_topics_items, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bindData(context, position);


    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }



    public  class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;

        public ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_tittle);

        }

        void bindData(Context context, int position) {
            tvTitle.setText(listingDetails.get(position).getTitle());
        }
    }


}