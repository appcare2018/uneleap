package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.AnswerByQuestionItemBinding;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.GetAnswerByQuestionResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.LIKEDISLIKE;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.apps.uneleap.util.Utils.convertDate;

/**
 * Created by  on 01-06-2019.
 * Simple adapter class, used for show all numbers in list
 */
public class ForumAnswerByQuestionAdapter extends RecyclerView.Adapter<ForumAnswerByQuestionAdapter.ViewHolder>  {

    private ArrayList<GetAnswerByQuestionResponseModel.Data> listingDetails;
    private Context context;
    private CommonFunctions cmf;
    public interface OnItemClickListener {
        void onItemClick(boolean isQuestionClick, int item);
    }
    private final OnItemClickListener listener;

    public ForumAnswerByQuestionAdapter(Context context, ArrayList<GetAnswerByQuestionResponseModel.Data> listingDetails, OnItemClickListener listener) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listener = listener;
        this.listingDetails = new ArrayList<>(listingDetails);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_page_topics_items, parent, false);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AnswerByQuestionItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.answer_by_question_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.bindData(position);


    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }



    public  class ViewHolder extends RecyclerView.ViewHolder {
      public   AnswerByQuestionItemBinding binding;


        public ViewHolder(AnswerByQuestionItemBinding v) {
            super(v.getRoot());
            binding =v;

        }

        void bindData(int position) {
            //binding.tvTitle.setText(listingDetails.get(position).getTitle());
            //binding.tvQuestion.setText(listingDetails.get(position).getQuestion());
            if(listingDetails.get(position).getAnswered_by()!=null){
                binding.tvName.setText(""+listingDetails.get(position).getAnswered_by().getFirst_name()+" "+
                        listingDetails.get(position).getAnswered_by().getLast_name());

            }

            if(listingDetails.get(position).getAnswer()!=null){
                try {
                    Glide.with(context)
                            .load(listingDetails.get(position).getAnswered_by().getImage_url())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .placeholder(R.drawable.profile_image)
                            .error(R.drawable.profile_image)
                            .into(binding.profilePic);



                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                binding.tvAnswer.setText(listingDetails.get(position).getAnswer());
                binding.tvLike.setText(listingDetails.get(position).getLikes());
                binding.tvComment.setText(listingDetails.get(position).getComments());
                binding.tvAnsweredDate.setText("Answered "+convertDate(listingDetails.get(position).getPosted_date(),"MMM dd yyyy"));

                if (listingDetails.get(position).isLiked_by_you())
                    binding.ivLike.setChecked(true);
                binding.ivLike.setOnCheckedChangeListener((buttonView, isChecked) -> {

                    if (listingDetails.get(position).getAnswer_id()!=null){
                        if (isChecked)
                            likeDislike(listingDetails.get(position).getAnswer_id(),"1");
                        else
                            likeDislike(listingDetails.get(position).getAnswer_id(),"0");
                    }


                });
            }

           // binding.tvQuestion.setOnClickListener(view -> listener.onItemClick(true,Integer.parseInt(listingDetails.get(position).getQuestion_id())));

            //binding.llLikeComment.setOnClickListener(view ->listener.onItemClick(false,Integer.parseInt(listingDetails.get(position).getQuestion_id())));
        }
    }

    private void likeDislike(String answerId,String isLike){

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis();

            params.put("user_id", CommonFunctions.myPreference.getString(context, USER_ID));
            params.put("answer_id", answerId);
            params.put("like", isLike);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.likeDislikeAnswer(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(context, new ApiResponseErrorCallback() {
                        @Override
                        public void getApiResponse(Object object, int flag) {
                            BaseResponseModel responseModel = (BaseResponseModel) object;
                            if(responseModel.getStatus()==200){
                                Utils.showToast(context, responseModel.getMessage());
                            }
                        }

                        @Override
                        public void getApiError(Throwable t, int flag) {

                        }
                    }, LIKEDISLIKE, true)
            );
        }else {
            Utils.showToast(context, context.getResources().getString(R.string.no_internet_error));
        }
    }
}