package com.apps.uneleap.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.controller.callBack.ViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Om on 17-01-2020.
 */

public class AddRecyclerViewAdapter<T extends ViewModel> extends RecyclerView.Adapter<AddRecyclerViewAdapter.ViewHolder> {

    private List<? extends T> list;
    private T viewModel;


    public AddRecyclerViewAdapter(List<? extends T> list, T viewModel) {
        this.list = list;
        this.viewModel = viewModel;
    }

    public AddRecyclerViewAdapter(List<? extends T> list) {
        this.list = list;
    }

    public static class ViewHolder<V extends ViewDataBinding> extends RecyclerView.ViewHolder {
        private V v;

        public ViewHolder(V v) {
            super(v.getRoot());
            this.v = v;
        }

        public V getBinding() {
            return v;
        }
    }



    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        ViewDataBinding bind = DataBindingUtil.bind(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false));
        return new ViewHolder<>(bind);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        final T model = list.get(position);
        holder.getBinding().setVariable(com.apps.uneleap.BR.model, model);

        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return 10;
    }

}