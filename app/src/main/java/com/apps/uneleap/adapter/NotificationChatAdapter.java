package com.apps.uneleap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.uneleap.R;
import com.apps.uneleap.databinding.ChatAdapterBinding;
import com.apps.uneleap.databinding.NotificationChatItemBinding;
import com.apps.uneleap.model.NotificationChatModel;
import com.apps.uneleap.util.CommonFunctions;

import java.util.ArrayList;

public class NotificationChatAdapter extends RecyclerView.Adapter<NotificationChatAdapter.ViewHolder> {

    private ArrayList<NotificationChatModel> listingDetails;
    private Context context;
    private CommonFunctions cmf;

    NotificationChatItemBinding notificationChatItemBinding;

    public NotificationChatAdapter(Context context, ArrayList<NotificationChatModel> listingDetails) {
        this.context = context;
        cmf = new CommonFunctions(context);
        this.listingDetails = listingDetails;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.notification_chat_item, parent, false);
        notificationChatItemBinding = DataBindingUtil.bind(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
//        holder.bindData(context, position);
       notificationChatItemBinding.setNotificationChatModel(listingDetails.get(position));
       notificationChatItemBinding.executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return listingDetails.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View v) {
            super(v);

        }

        void bindData(Context context, int position) {

        }
    }

}