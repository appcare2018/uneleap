package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    String[] searchArray;
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm, String[] searchArray) {
        super(fm);
        mContext = context;
        this.searchArray = searchArray;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return NewsPagerFragment.newInstance(position, searchArray[position]);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return searchArray[position];
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return searchArray.length;
    }
}