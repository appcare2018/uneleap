package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.os.Bundle;

import com.apps.uneleap.R;
import com.apps.uneleap.fragement.BaseFragment;


/**
 * A placeholder fragment containing a simple view.
 */
public class VideosFragment extends BaseFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static VideosFragment newInstance(int index) {
        VideosFragment fragment = new VideosFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected void initUi() {
    }

    @Override
    protected int getLayoutById() {
        return R.layout.videos_fr;
    }
}