package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.os.Bundle;

import com.apps.uneleap.R;
import com.apps.uneleap.controller.BaseFragment;
import com.apps.uneleap.databinding.NewsPagerFrBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewsPagerFragment extends BaseFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_TITLE = "title";


    public static NewsPagerFragment newInstance(int index, String title) {
        NewsPagerFragment fragment = new NewsPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putString(ARG_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    NewsPagerFrBinding binding;

    @Override
    protected void initUi() {
        binding = (NewsPagerFrBinding) viewDataBinding;
        // binding.rcvList.setAdapter(new HomeControllerAdapter(getContext(), R.layout.content_item,cmf));
        int index = 1;
        String title = "";
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            title = getArguments().getString(ARG_TITLE);
        }
        // binding.label.setText(ARG_SECTION_NUMBER + index);
        // cmf.logD(binding.label.getText().toString() + title);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.news_pager_fr;
    }


}