package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.AddRecyclerViewAdapter;
import com.apps.uneleap.adapter.NewsPagePagerAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiInterface;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.controller.BaseFragment;
import com.apps.uneleap.controller.pojo.NotificationModel;
import com.apps.uneleap.databinding.WorldWideFrBinding;

import java.util.ArrayList;
import java.util.List;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.ADDQUESTIONS;


/**
 * A placeholder fragment containing a simple view.
 */
public class WordWideFragment extends BaseFragment implements ApiResponseErrorCallback{
    private static final String ARG_SECTION_NUMBER = "section_number";


    public static WordWideFragment newInstance(int index) {
        WordWideFragment fragment = new WordWideFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    private WorldWideFrBinding binding;

    @Override
    protected void initUi() {
        binding = (WorldWideFrBinding) viewDataBinding;
        initview();
    }

    private void initview() {

        ArrayList<String> item = new ArrayList<>();
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");

        /*binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AddRecyclerViewAdapter(list);
        binding.recyclerView.setAdapter(adapter);*/

      //  getNotificationList();
    }


    public static WordWideFragment newInstance() {
        return new WordWideFragment();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.world_wide_fr;
    }


    private List<NotificationModel.Notifications> list = new ArrayList<>();
    private AddRecyclerViewAdapter adapter;

    //------------------------------
    private void getNotificationList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        apiService.newsWorldWide(cmf.getFAq("", ""))
                .enqueue(new ApiCallBack<>(getActivity(), this, ADDQUESTIONS,true));
    }

    @Override
    public void getApiResponse(Object object, int flag) {
        NotificationModel model = (NotificationModel) object;
        if (model.status){
            list.addAll(model.result);
            adapter.notifyDataSetChanged();
        }else {
            showToast(model.message);
        }
    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }


}