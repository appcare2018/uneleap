package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.apps.uneleap.R;
import com.apps.uneleap.adapter.AddRecyclerViewAdapter;
import com.apps.uneleap.adapter.NewsPagePagerAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiInterface;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.controller.BaseFragment;
import com.apps.uneleap.controller.pojo.NotificationModel;
import com.apps.uneleap.databinding.TopStoryFrBinding;

import java.util.ArrayList;
import java.util.List;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.ADDQUESTIONS;


/**
 * A placeholder fragment containing a simple view.
 */

public class TopStoryFragment extends BaseFragment implements ApiResponseErrorCallback {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static TopStoryFragment newInstance(int index) {
        TopStoryFragment fragment = new TopStoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutById() {
        return R.layout.top_story_fr;
    }

    TopStoryFrBinding binding;

    @Override
    protected void initUi() {
        binding = (TopStoryFrBinding) viewDataBinding;

        initview();
    }

    private void initview() {


        ArrayList<String> item = new ArrayList<>();
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");




        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getActivity().getSupportFragmentManager(),
                getResources().getStringArray(R.array.news_array));

        binding.viewPager.setAdapter(sectionsPagerAdapter);
        binding.dotsIndicator.setViewPager(binding.viewPager);


    }



    private List<NotificationModel.Notifications> list = new ArrayList<>();
    private AddRecyclerViewAdapter adapter;

    //------------------------------
    private void getNotificationList() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        apiService.newsWorldWide(cmf.getFAq("", ""))
                .enqueue(new ApiCallBack<>(getActivity(), this, ADDQUESTIONS, true));
    }

    @Override
    public void getApiResponse(Object object, int flag) {

    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }



}