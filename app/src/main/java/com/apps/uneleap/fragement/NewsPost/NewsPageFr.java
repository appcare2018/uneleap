package com.apps.uneleap.fragement.NewsPost;

import com.apps.uneleap.R;
import com.apps.uneleap.activity.MainActivity;
import com.apps.uneleap.databinding.NewsPageBinding;
import com.apps.uneleap.fragement.BaseFragment;
import com.apps.uneleap.fragement.NewsPost.news_chield.NewsPagerAdapter;

import java.util.Objects;

public class NewsPageFr extends BaseFragment {

    private NewsPageBinding binding;

    @Override
    protected void initUi() {
        binding = (NewsPageBinding) viewDataBinding;

        ((MainActivity) Objects.requireNonNull(getActivity())).updateBottomTabColor(3);
        binding.tabsMain.setupWithViewPager(binding.vpMain);

        NewsPagerAdapter homePagerAdapter = new NewsPagerAdapter(getActivity(), getActivity().getSupportFragmentManager());
        binding.vpMain.setAdapter(homePagerAdapter);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.news_page;
    }


}
