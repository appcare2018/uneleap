package com.apps.uneleap.fragement;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.uneleap.util.Utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    private View view;
    protected Context context;
    protected ViewDataBinding viewDataBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);

       if (viewDataBinding != null)
           return viewDataBinding.getRoot();
       else return inflater.inflate(getLayoutById(), container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;

        initUi();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;



    }

    protected View findViewById(int resId){ return view.findViewById(resId);}

    /**
     * Initilize Ui parameters here
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     * @return Layout res id
     */
    protected abstract int getLayoutById();

    protected String getStringFromResource(int id){
        return context.getResources().getString(id);
    }

    protected void showToast(String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    protected Drawable getDrawable(int id){
        return ContextCompat.getDrawable(context, id);
    }
}