package com.apps.uneleap.fragement.NewsPost.news_chield;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.apps.uneleap.R;


public class NewsPagerAdapter extends FragmentStatePagerAdapter {

    private String[] tabTitles;

    private final Context mContext;

    public NewsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);

        mContext = context;
        tabTitles = mContext.getResources().getStringArray(R.array.news_array);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1)
            return WordWideFragment.newInstance(position);
        else if (position == 2)
            return VideosFragment.newInstance(position);
        else if (position == 3)
            return NearByFragment.newInstance(position);
        else
            return TopStoryFragment.newInstance(position);
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}