package com.apps.uneleap.fragement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.apps.uneleap.R;
import com.apps.uneleap.activity.forum.ForumAnswerByQuestion;
import com.apps.uneleap.activity.forum.ForumComment;
import com.apps.uneleap.activity.MainActivity;
import com.apps.uneleap.activity.forum.ForumTopics;
import com.apps.uneleap.adapter.ForumPageQuestionsAdapter;
import com.apps.uneleap.adapter.ForumPageTopicsAdapter;
import com.apps.uneleap.apiPackage.ApiCallBack;
import com.apps.uneleap.apiPackage.ApiClient;
import com.apps.uneleap.apiPackage.ApiResponseErrorCallback;
import com.apps.uneleap.databinding.ForumPageBinding;
import com.apps.uneleap.model.AllTopicSpinnerModel;
import com.apps.uneleap.model.BaseResponseModel;
import com.apps.uneleap.model.GetQuestionResponseModel;
import com.apps.uneleap.model.QuestionResponseModel;
import com.apps.uneleap.model.TopicsResponseModel;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.util.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;

import static com.apps.uneleap.apiPackage.ApiRequestConstant.ADDQUESTIONS;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.API_INTERFACE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.LIKEDISLIKE;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.QUESTIONS;
import static com.apps.uneleap.apiPackage.ApiRequestConstant.TOPICS;
import static com.apps.uneleap.util.GlobalConstants.USER_ID;
import static com.apps.uneleap.util.Utils.convertDate;

public class ForumPageFr extends BaseFragment implements ApiResponseErrorCallback {

    private ForumPageBinding binding;
    CommonFunctions cmf;
    private ForumPageTopicsAdapter adapterTopics;
    private ForumPageQuestionsAdapter adapterQuestion;
    private float x1,x2;
    static final int MIN_DISTANCE = 350;
    private ArrayList<TopicsResponseModel.Data> topicDetails = new ArrayList<>();
    LinearLayoutManager  linearLayoutManager;
    private ArrayList<QuestionResponseModel.Data> questionDetails = new ArrayList<>();
    private ArrayList<QuestionResponseModel.Data> questionDetailsFirstIndex = new ArrayList<>();
    LinearLayoutManager  questionLinearLayoutManager;
    ArrayAdapter<AllTopicSpinnerModel> dataAdapter;
    private String selectedTopicItemId = "0";


    @Override
    protected void initUi() {
        binding = (ForumPageBinding) viewDataBinding;
        cmf = new CommonFunctions(context);
        ((MainActivity) Objects.requireNonNull(getActivity())).updateBottomTabColor(2);
        initview();
    }



    @Override
    protected int getLayoutById() {
        return R.layout.forum_page;
    }

    private void initview() {

        linearLayoutManager = new LinearLayoutManager(context,  LinearLayoutManager.HORIZONTAL, false);
        binding.rcvTopics.setLayoutManager(linearLayoutManager);

        questionLinearLayoutManager = new LinearLayoutManager(context,  LinearLayoutManager.VERTICAL, false);
        binding.rcvQuestion.setLayoutManager(questionLinearLayoutManager);


        binding.btnAdd.setOnClickListener(v -> {showAddDialog();});

//        binding.llLikeComment.setOnClickListener(view -> {
//            startActivity(new Intent(getActivity(), ForumComment.class));
//            getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//        });
        binding.viewAll.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), ForumTopics.class));
            getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });



        getQuestions();
        getTopics();
    }

    private void showAddDialog() {




        final Dialog dialog = new Dialog(getActivity(),R.style.MyCustomDialog);
        dialog.setCancelable(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_write_question);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ImageView ivClose =  dialog.findViewById(R.id.ivClose);
        final Button btnAdd =  dialog.findViewById(R.id.btnAdd);
        final EditText etQue =  dialog.findViewById(R.id.etQue);

        final Spinner spTopics =  dialog.findViewById(R.id.spTopics);

        dataAdapter = new ArrayAdapter<>(getActivity(),  R.layout.spinner_layout, getListData());
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spTopics.setAdapter(dataAdapter);

        spTopics.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                AllTopicSpinnerModel catDetails = dataAdapter.getItem(position);
                    selectedTopicItemId = catDetails.getId();
//                    selectedCategoryItemValue = catDetails.getValue();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivClose.setOnClickListener(v -> dialog.dismiss());
        btnAdd.setOnClickListener(v -> {
            addQuestion(Integer.parseInt(selectedTopicItemId),etQue.getText().toString());
            dialog.dismiss();
        });



        dialog.show();

    }

    public List<AllTopicSpinnerModel> getListData() {
        List<AllTopicSpinnerModel> imageList = new ArrayList<>();

        for (int i=0;i<topicDetails.size();i++){
                imageList.add(new AllTopicSpinnerModel(topicDetails.get(i).getTopic_id(),topicDetails.get(i).getTitle()));
        }

        // return contact list
        return imageList;
    }

    private void addQuestion(int topicId,String question){

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis()/1000;

            params.put("user_id", CommonFunctions.myPreference.getString(getActivity(), USER_ID));
            params.put("topic_id", topicId);
            params.put("question", question);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.addQuestion(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(getActivity(), this, ADDQUESTIONS,true)
            );
        }else {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_internet_error));
        }
    }

    private void likeDislike(int answerId,String isLike){

        JSONObject params = new JSONObject();
        try {

            Long tsLong = System.currentTimeMillis();

            params.put("user_id", CommonFunctions.myPreference.getString(getActivity(), USER_ID));
            params.put("answer_id", answerId);
            params.put("like", isLike);
            params.put("edate", tsLong);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.likeDislikeAnswer(
                    ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(getActivity(), this, LIKEDISLIKE,true)
            );
        }else {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_internet_error));
        }
    }

    private void getQuestions() {
        JSONObject params = new JSONObject();
        try {

            params.put("user_id", CommonFunctions.myPreference.getString(getActivity(), USER_ID));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getQuestion(ApiClient.getRequestValue(params)).enqueue(
                    new ApiCallBack<>(getActivity(), this, QUESTIONS,true)
            );
        }else {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_internet_error));
        }
    }

    private void getTopics() {

        if (cmf.cdr.isConnectingToInternet()) {
            API_INTERFACE.getAllTopics().enqueue(
                    new ApiCallBack<>(getActivity(), this, TOPICS,true)
            );
        }else {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getApiResponse(Object object, int flag) {
        if(flag == TOPICS){
            TopicsResponseModel responseModel = (TopicsResponseModel) object;
            if (responseModel.getStatus() == 200) {
                if (responseModel.getData() != null) {
                    if (responseModel.getData().size() != 0) {

                        topicDetails = new ArrayList<>();
                        topicDetails.addAll(responseModel.getData());
                        adapterTopics = new ForumPageTopicsAdapter(context, topicDetails);
                        binding.rcvTopics.setAdapter(adapterTopics);


                    }
                }
            }
        }

        if(flag == QUESTIONS){
            QuestionResponseModel responseModel = (QuestionResponseModel) object;
            if (responseModel.getStatus() == 200) {
                if (responseModel.getData() != null) {
                    if (responseModel.getData().size() != 0) {

                        questionDetailsFirstIndex = new ArrayList<>();
                        questionDetailsFirstIndex.addAll(responseModel.getData());
                        questionDetails = new ArrayList<>();
                        questionDetails.addAll(responseModel.getData());


                        if (questionDetailsFirstIndex.get(0).getMost_like_answer().isLiked_by_you())
                            binding.questionFirstIndexItem.ivLike.setChecked(true);


                        binding.questionFirstIndexItem.tvQuestionIndex.setText(questionDetailsFirstIndex.get(0).getQuestion());
                        if(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswered_by()!=null){

                            binding.questionFirstIndexItem.tvNameIndex.setText(
                                    ""+questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswered_by().getFirst_name()+" "+
                                    questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswered_by().getLast_name());

                        }

                        if(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswer()!=null){

                            try {
                                Glide.with(this)
                                        .load(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswered_by().getImage_url())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .placeholder(R.drawable.profile_image)
                                        .error(R.drawable.profile_image)
                                        .into(binding.questionFirstIndexItem.profilepicIndex);



                            }
                            catch (Exception ex){
                                ex.printStackTrace();
                            }

                            binding.questionFirstIndexItem.tvAnswerIndex.setText(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswer());
                            binding.questionFirstIndexItem.tvLikeIndex.setText(questionDetailsFirstIndex.get(0).getMost_like_answer().getLikes());
                            binding.questionFirstIndexItem.tvCommentIndex.setText(questionDetailsFirstIndex.get(0).getMost_like_answer().getComments());
                            binding.questionFirstIndexItem.tvAnsweredDateIndex.setText("Answered "+convertDate(questionDetails.get(0).getMost_like_answer().getEdate(),"MMM dd yyyy"));
                        }


                        binding.questionFirstIndexItem.tvQuestionIndex.setOnClickListener(view -> {
                            int id =Integer.parseInt(questionDetailsFirstIndex.get(0).getQuestion_id());
                            Intent intent = new Intent(getActivity(), ForumAnswerByQuestion.class);
                            intent.putExtra("id",id);
                            intent.putExtra("question",questionDetailsFirstIndex.get(0).getQuestion());
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        });

                        binding.questionFirstIndexItem.tvCommentIndex.setOnClickListener(view -> {
                            Intent intent=new Intent(getActivity(),ForumComment.class);
                            intent.putExtra("QuestionResponseModel", questionDetailsFirstIndex.get(0));
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        });

                        binding.questionFirstIndexItem.ivLike.setOnCheckedChangeListener((buttonView, isChecked) -> {

                            if (questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswer_id()!=null){
                                if (isChecked)
                                    likeDislike(Integer.parseInt(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswer_id()),"1");
                                else
                                    likeDislike(Integer.parseInt(questionDetailsFirstIndex.get(0).getMost_like_answer().getAnswer_id()),"0");
                            }


                        });


                        //removing 0 index item
                        questionDetails.remove(0);
                        //--------
                        adapterQuestion = new ForumPageQuestionsAdapter(context, questionDetails, (isClick,isLike ,listingDetails) -> {
                            if (isClick==0){
                                int id =Integer.parseInt(listingDetails.getQuestion_id());
                                Intent intent = new Intent(getActivity(), ForumAnswerByQuestion.class);
                                intent.putExtra("id",id);
                                intent.putExtra("question",listingDetails.getQuestion());
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                            }else if (isClick==2){
                               // startActivity(new Intent(getActivity(), ForumComment.class));

                                Intent intent=new Intent(getActivity(),ForumComment.class);
                                intent.putExtra("QuestionResponseModel", listingDetails);
                                startActivity(intent);

                                getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                            }

                            else if (isClick==3){
                                // startActivity(new Intent(getActivity(), ForumComment.class));
                               // Log.d("------>");
                                if (listingDetails.getMost_like_answer().getAnswer_id()!=null)
                                    likeDislike(Integer.parseInt(listingDetails.getMost_like_answer().getAnswer_id()),isLike);
                            }

                        });
                        binding.rcvQuestion.setAdapter(adapterQuestion);
                    }
                }
            }
        }

        if(flag == ADDQUESTIONS){
            BaseResponseModel responseModel = (BaseResponseModel) object;
            if (responseModel.getStatus() == 200) {
                Utils.showToast(getActivity(),responseModel.getMessage());
            }
        }

        if(flag == LIKEDISLIKE){
            BaseResponseModel responseModel = (BaseResponseModel) object;
            if (responseModel.getStatus() == 200) {
                Utils.showToast(getActivity(),responseModel.getMessage());
            }
        }

    }

    @Override
    public void getApiError(Throwable t, int flag) {

    }
}
