package com.apps.uneleap.fragement;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.apps.uneleap.R;
import com.apps.uneleap.activity.MainActivity;
import com.apps.uneleap.activity.Tools;
import com.apps.uneleap.adapter.EventPageEventAdapter;
import com.apps.uneleap.adapter.LibraryPageLibraryAdapter;
import com.apps.uneleap.databinding.EventPageBinding;
import com.apps.uneleap.databinding.LibraryPageBinding;
import com.apps.uneleap.util.CommonFunctions;
import com.apps.uneleap.viewpager.DotIndicatorPagerAdapter;

import java.util.ArrayList;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;

public class LibraryPageFr extends BaseFragment {

    private LibraryPageBinding binding;
    CommonFunctions cmf;
    private LibraryPageLibraryAdapter adapterCommunity;
    DotIndicatorPagerAdapter adapter;
    private float x1,x2;
    static final int MIN_DISTANCE = 350;



    @Override
    protected void initUi() {
        binding = (LibraryPageBinding) viewDataBinding;
        cmf = new CommonFunctions(context);
        ((MainActivity) Objects.requireNonNull(getActivity())).updateBottomTabColor(6);
        initview();
        tabBar();
//        binding.topScv.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                switch(event.getAction())
//                {
//                    case MotionEvent.ACTION_DOWN:
//                        x1 = event.getX();
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        x2 = event.getX();
//                        float deltaX = x2 - x1;
//
//                        if (Math.abs(deltaX) > MIN_DISTANCE)
//                        {
//                            // Left to Right swipe action
//                            if (x2 > x1)
//                            {
//                                startActivity(new Intent(context, Tools.class));
//                                getActivity().overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
//
//                                //Toast.makeText(context, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
//                            }
//
//                            // Right to left swipe action
//                            else
//                            {
//                               // Toast.makeText(context, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
//                            }
//
//                        }
//                        else
//                        {
//                            // consider as something else - a screen tap for example
//                        }
//                        break;
//                }
//                return true;
//            }
//        });
    }

    private void tabBar() {
        binding.communityPageTopTab.profileGeneralTab.setText("Recommendations");
        binding.communityPageTopTab.worldwideTab.setText("Your Collection");
    }


    @Override
    protected int getLayoutById() {
        return R.layout.library_page;
    }

    private void initview() {

        ArrayList<String> item=new ArrayList<>();
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");
        item.add("fgg");


      LinearLayoutManager  linearLayoutManager = new LinearLayoutManager(context,  LinearLayoutManager.HORIZONTAL, false);
        binding.rcvCommunities.setLayoutManager(linearLayoutManager);
        adapterCommunity = new LibraryPageLibraryAdapter(context, item);
        binding.rcvCommunities.setAdapter(adapterCommunity);
        //binding.dotsIndicator.setViewPager(binding.rcvTopics);

    }

}
